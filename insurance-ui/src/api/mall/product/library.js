import request from '@/utils/request'
import {addSystemAndSite, addSystemAndSiteAndLibrary} from "@/utils/xueyi";

// 查询产品库选项
export function selectLibrary(data) {
  return request({
    url: '/product/library/select',
    method: 'get',
    params: addSystemAndSite(data)
  })
}

// 查询产品库列表
export function listLibrary(data) {
  return request({
    url: '/product/library/list',
    method: 'get',
    params: addSystemAndSite(data)
  })
}

// 查询产品库详细
export function getLibrary(data) {
  return request({
    url: '/product/library/byId',
    method: 'get',
    params: addSystemAndSite(data)
  })
}

// 新增产品库
export function addLibrary(data) {
  return request({
    url: '/product/library',
    method: 'post',
    data: addSystemAndSite(data)
  })
}

// 修改产品库
export function updateLibrary(data) {
  return request({
    url: '/product/library',
    method: 'put',
    data: addSystemAndSite(data)
  })
}

// 修改产品参数排序
export function updateLibrarySort(data) {
  return request({
    url: '/product/library/sort',
    method: 'put',
    data: addSystemAndSite(data)
  })
}

// 删除产品库
export function delLibrary(data) {
  return request({
    url: '/product/library',
    method: 'delete',
    data: addSystemAndSite(data)
  })
}
