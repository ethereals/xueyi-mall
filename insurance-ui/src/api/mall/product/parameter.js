import request from '@/utils/request'
import {addSystemAndSiteAndLibrary} from "@/utils/xueyi";

// 查询产品参数列表赋予产品
export function listParameterToProduct(data) {
  return request({
    url: '/product/parameter/listToProduct',
    method: 'get',
    params: addSystemAndSiteAndLibrary(data)
  })
}

// 查询产品参数列表
export function listParameter(data) {
  return request({
    url: '/product/parameter/list',
    method: 'get',
    params: addSystemAndSiteAndLibrary(data)
  })
}

// 查询产品参数详细
export function getParameter(data) {
  return request({
    url: '/product/parameter/byId',
    method: 'get',
    params: addSystemAndSiteAndLibrary(data)
  })
}

// 新增产品参数
export function addParameter(data) {
  return request({
    url: '/product/parameter',
    method: 'post',
    data: addSystemAndSiteAndLibrary(data)
  })
}

// 修改产品参数
export function updateParameter(data) {
  return request({
    url: '/product/parameter',
    method: 'put',
    data: addSystemAndSiteAndLibrary(data)
  })
}

// 修改产品参数排序
export function updateParameterSort(data) {
  return request({
    url: '/product/parameter/sort',
    method: 'put',
    data: addSystemAndSiteAndLibrary(data)
  })
}

// 删除产品参数
export function delParameter(data) {
  return request({
    url: '/product/parameter',
    method: 'delete',
    data: addSystemAndSiteAndLibrary(data)
  })
}
