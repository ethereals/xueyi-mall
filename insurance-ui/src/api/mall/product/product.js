import request from '@/utils/request'
import {addSystemAndSiteAndLibrary} from "@/utils/xueyi";

// 查询产品列表
export function listProduct(query) {
  return request({
    url: '/product/product/list',
    method: 'get',
    params: addSystemAndSiteAndLibrary(query)
  })
}


// 查询产品详细
export function getProduct(query) {
  return request({
    url: '/product/product/byId',
    method: 'get',
    params: addSystemAndSiteAndLibrary(query)
  })
}

// 新增产品
export function addProduct(data) {
  return request({
    url: '/product/product',
    method: 'post',
    data: addSystemAndSiteAndLibrary(data)
  })
}

// 修改产品
export function updateProduct(data) {
  return request({
    url: '/product/product',
    method: 'put',
    data: addSystemAndSiteAndLibrary(data)
  })
}

// 修改产品排序
export function updateProductSort(data) {
  return request({
    url: '/product/product/sort',
    method: 'put',
    data: addSystemAndSiteAndLibrary(data)
  })
}

// 删除产品
export function delProduct(data) {
  return request({
    url: '/product/product',
    method: 'delete',
    data: addSystemAndSiteAndLibrary(data)
  })
}
