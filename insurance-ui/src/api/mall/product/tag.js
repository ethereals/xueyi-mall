import request from '@/utils/request'
import { addSystemAndSite } from "@/utils/xueyi";

// 查询产品标签列表
export function listTag(query) {
  return request({
    url: '/product/tag/list',
    method: 'get',
    params: addSystemAndSite(query)
  })
}


// 查询产品标签详细
export function getTag(query) {
  return request({
    url: '/product/tag/byId',
    method: 'get',
    params: addSystemAndSite(query)
  })
}

// 新增产品标签
export function addTag(data) {
  return request({
    url: '/product/tag',
    method: 'post',
    data: addSystemAndSite(data)
  })
}

// 修改产品标签
export function updateTag(data) {
  return request({
    url: '/product/tag',
    method: 'put',
    data: addSystemAndSite(data)
  })
}

// 修改产品标签排序
export function updateTagSort(data) {
  return request({
    url: '/product/tag/sort',
    method: 'put',
    data: addSystemAndSite(data)
  })
}

// 删除产品标签
export function delTag(data) {
  return request({
    url: '/product/tag',
    method: 'delete',
    data: addSystemAndSite(data)
  })
}
