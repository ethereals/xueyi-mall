import request from '@/utils/request'

// 获取路由
export function getRouters(systemId){
  return request({
    url: '/system/menu/getRouters/'+systemId,
    method: 'get'
  })
}
