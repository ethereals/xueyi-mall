module.exports = {
  title: '雪忆商城系统',

  /**
   * 侧边栏主题 深色主题theme-dark，浅色主题theme-light
   */
  sideTheme: 'theme-dark',

  /**
   * 是否系统布局配置
   */
  showSettings: false,

  /**
   * 是否显示顶部导航
   */
  topNav: false,

  /**
   * 是否显示 tagsView
   */
  tagsView: false,

  /**
   * 是否固定头部
   */
  fixedHeader: true,

  /**
   * 是否显示logo
   */
  sidebarLogo: true,

  /**
   * 产品库编号
   */
  libraryId: 0,

  /**
   * 站点编号
   */
  siteId: 0,

  /**
   * 系统编号
   */
  systemId: 1,

  /**
   * 初始页名称|图标
   */
  homePageName: '概况',
  homePageIcon: 'xy_productCenter',

  /**
   * 基础系统地址
   */
  baseSystemUrl: 'http://localhost',

  /**
   * @type {string | array} 'production' | ['production', 'development']
   * @description Need show err logs component.
   * The default is only used in the production env
   * If you want to also use it in dev, you can pass ['production', 'development']
   */
  errorLog: 'production'
}
