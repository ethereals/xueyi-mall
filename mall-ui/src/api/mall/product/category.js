import request from '@/utils/request'
import {addSystemAndSite} from "@/utils/xueyi";

// 查询产品分类列表
export function listCategory(query) {
  return request({
    url: '/product/category/list',
    method: 'get',
    params: addSystemAndSite(query)
  })
}

// 查询产品分类树选项
export function treeCategory(query) {
  return request({
    url: '/product/category/tree',
    method: 'get',
    params: addSystemAndSite(query)
  })
}

// 查询产品分类详细
export function getCategory(query) {
  return request({
    url: '/product/category/byId',
    method: 'get',
    params: addSystemAndSite(query)
  })
}

// 新增产品分类
export function addCategory(data) {
  return request({
    url: '/product/category',
    method: 'post',
    data: addSystemAndSite(data)
  })
}

// 修改产品分类
export function updateCategory(data) {
  return request({
    url: '/product/category',
    method: 'put',
    data: addSystemAndSite(data)
  })
}

// 删除产品分类
export function delCategory(data) {
  return request({
    url: '/product/category',
    method: 'delete',
    data: addSystemAndSite(data)
  })
}
