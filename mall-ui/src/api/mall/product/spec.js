import request from '@/utils/request'
import {addSystemAndSite, addSystemAndSiteAndLibrary} from "@/utils/xueyi";
import {listParameterToProduct} from "@/api/mall/product/parameter";

// 查询产品规格列表赋予产品
export function listSpecToProduct(data) {
  return request({
    url: '/product/spec/listToProduct',
    method: 'get',
    params: addSystemAndSiteAndLibrary(data)
  })
}

// 查询产品规格列表
export function listSpec(data) {
  return request({
    url: '/product/spec/list',
    method: 'get',
    params: addSystemAndSiteAndLibrary(data)
  })
}

// 查询产品规格详细
export function getSpec(data) {
  return request({
    url: '/product/spec/byId',
    method: 'get',
    params: addSystemAndSiteAndLibrary(data)
  })
}

// 新增产品规格
export function addSpec(data) {
  return request({
    url: '/product/spec',
    method: 'post',
    data: addSystemAndSiteAndLibrary(data)
  })
}

// 修改产品规格
export function updateSpec(data) {
  return request({
    url: '/product/spec',
    method: 'put',
    data: addSystemAndSiteAndLibrary(data)
  })
}

// 修改产品规格排序
export function updateSpecSort(data) {
  return request({
    url: '/product/spec/sort',
    method: 'put',
    data: addSystemAndSiteAndLibrary(data)
  })
}

// 删除产品规格
export function delSpec(data) {
  return request({
    url: '/product/spec',
    method: 'delete',
    data: addSystemAndSiteAndLibrary(data)
  })
}
