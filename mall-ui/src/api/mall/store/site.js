import request from '@/utils/request'

// 查询商城-站点模块列表
export function listStore(query) {
  return request({
    url: '/store/site/list',
    method: 'get',
    params: query
  })
}

// 查询商城-站点模块详细
export function getStore(query) {
  return request({
    url: '/store/site/byId',
    method: 'get',
    params: query
  })
}

// 新增商城-站点模块
export function addStore(data) {
  return request({
    url: '/store/site',
    method: 'post',
    data: data
  })
}

// 修改商城-站点模块
export function updateStore(data) {
  return request({
    url: '/store/site',
    method: 'put',
    data: data
  })
}

// 删除商城-站点模块
export function delStore(data) {
  return request({
    url: '/store/site',
    method: 'delete',
    data: data
  })
}
