/**
 * 通用js方法封装处理
 * Copyright (c) 2021 xueyi
 */

const baseURL = process.env.VUE_APP_BASE_API;
import defaultSettings from '@/settings';

/** 添加系统参数 | 系统Id systemId */
export function addSystem(data) {
  const {systemId} = defaultSettings
  const state = {
    systemId: systemId
  }
  let search;
  if(data != null){
    search = data;
  }else {
    search={};
  }
  search["systemId"] = state.systemId;
  return search;
}
/** 添加系统参数 | 站点Id siteId */
export function addSite(data) {
  const {siteId} = defaultSettings
  const siteSetting = localStorage.getItem('site-setting') !== null ? JSON.parse(localStorage.getItem('site-setting').replace(/\"siteId\":(\d+)/, '"siteId": "$1"')) || '' : '';
  const state = {
    siteId: siteSetting.siteId === undefined ? siteId : siteSetting.siteId,
  }
  let search;
  if(data != null){
    search = data;
  }else {
    search={};
  }
  search["siteId"] = state.siteId;
  return search;
}
/** 添加系统参数 | 产品库Id libraryId */
export function addLibrary(data) {
  const {libraryId} = defaultSettings
  const librarySetting = localStorage.getItem('library-setting') !== null ? JSON.parse(localStorage.getItem('library-setting').replace(/\"libraryId\":(\d+)/, '"libraryId": "$1"')) || '' : '';
  const state = {
    libraryId: librarySetting.libraryId === undefined ? libraryId : librarySetting.libraryId,
  }
  let search;
  if(data != null){
    search = data;
  }else {
    search={};
  }
  search["libraryId"] = state.libraryId;
  return search;
}
/** 添加系统参数 | 系统Id systemId | 站点Id siteId */
export function addSystemAndSite(data) {
  const {systemId, siteId} = defaultSettings
  const siteSetting = localStorage.getItem('site-setting') !== null ? JSON.parse(localStorage.getItem('site-setting').replace(/\"siteId\":(\d+)/, '"siteId": "$1"')) || '' : '';
  const state = {
    siteId: siteSetting.siteId === undefined ? siteId : siteSetting.siteId,
    systemId: systemId
  }
  let search;
  if(data != null){
    search = data;
  }else {
    search={};
  }
  search["siteId"] = state.siteId;
  search["systemId"] = state.systemId;
  return search;
}
/** 添加系统参数 | 站点Id siteId | 产品库Id libraryId */
export function addSiteAndLibrary(data) {
  const {siteId, libraryId} = defaultSettings
  const siteSetting = localStorage.getItem('site-setting') !== null ? JSON.parse(localStorage.getItem('site-setting').replace(/\"siteId\":(\d+)/, '"siteId": "$1"')) || '' : '';
  const librarySetting = localStorage.getItem('library-setting') !== null ? JSON.parse(localStorage.getItem('library-setting').replace(/\"libraryId\":(\d+)/, '"libraryId": "$1"')) || '' : '';
  const state = {
    siteId: siteSetting.siteId === undefined ? siteId : siteSetting.siteId,
    libraryId: librarySetting.libraryId === undefined ? libraryId : librarySetting.libraryId
  }
  let search;
  if(data != null){
    search = data;
  }else {
    search={};
  }
  search["siteId"] = state.siteId;
  search["libraryId"] = state.libraryId;
  return search;
}
/** 添加系统参数 | 系统Id systemId | 站点Id siteId | 产品库Id libraryId */
export function addSystemAndSiteAndLibrary(data) {
  const {systemId, siteId, libraryId} = defaultSettings
  const siteSetting = localStorage.getItem('site-setting') !== null ? JSON.parse(localStorage.getItem('site-setting').replace(/\"siteId\":(\d+)/, '"siteId": "$1"')) || '' : '';
  const librarySetting = localStorage.getItem('library-setting') !== null ? JSON.parse(localStorage.getItem('library-setting').replace(/\"libraryId\":(\d+)/, '"libraryId": "$1"')) || '' : '';
  const state = {
    siteId: siteSetting.siteId === undefined ? siteId : siteSetting.siteId,
    libraryId: librarySetting.libraryId === undefined ? libraryId : librarySetting.libraryId,
    systemId: systemId
  }

  let search;
  if(data != null){
    search = data;
  }else {
    search={};
  }
  search["systemId"] = state.systemId;
  search["siteId"] = state.siteId;
  search["libraryId"] = state.libraryId;
  return search;
}
/** 数组转对象 */
export function updateParamIds(Ids, params, propName) {
  let search;
  if (params != null) {
    search = params;
    if (!search.hasOwnProperty('params')) {
      search["params"] = {}
    }
  } else {
    search = {params: {}};
  }
  if (null != Ids && '' != Ids) {
    if (typeof (Ids) === 'string') {
      Ids = Ids.split(",");
    }
    if (typeof (propName) === "undefined") {
      search.params["Ids"] = Ids;
    } else {
      search.params[propName] = Ids;
    }
  }
  return search;
}
/** 数组对象排序 | 需有sort参数 | 返回新排序数组 | 仅排序变化数据 */
export function sortOrderListOnlyDynamic(newList, oldList, idName) {
  let returnList = [];
  let id = idName != null ? idName : 'id';
  let listNew = JSON.parse(JSON.stringify(newList));
  for (let i = 0; i < listNew.length; i++) {
    listNew[i].sort = (i - 128) < 127 ? (i - 128) : 127;
    for (let j = 0; j < newList.length; j++) {
      if (listNew[i][id] === newList[j][id] && listNew[i].sort !== newList[j].sort) {
        returnList.push(listNew[i]);
      }
    }
  }
  return returnList;
}
/** 数组对象排序 | 需有sort参数 | 返回新排序数组 全部数据 */
export function sortOrderList(newList) {
  let listNew = JSON.parse(JSON.stringify(newList));
  for (let i = 0; i < listNew.length; i++) {
    listNew[i].sort = (i - 128) < 127 ? (i - 128) : 127;
  }
  return listNew;
}


/**  table合并行通用（不相邻的数据相互不受影响） */
export function mergeTableRow(config) {
  let data = config.data
  const { mergeColNames, firstMergeColNames, firstMerge } = config
  if (!mergeColNames || mergeColNames.length === 0) {
    return data
  }
  mergeColNames.forEach((m) => {
    const mList = {}
    data = data.map((v, index) => {
      const rowVal = v[m]
      if (mList[rowVal] && mList[rowVal].newIndex === index) {
        const flag = firstMergeColNames.filter((f) => { return f === m }).length !== 0
        const mcFlag = mergeColNames.filter((mc) => { return mc === firstMerge }).length === 0
        if ((mcFlag && flag) || (flag && data[index][firstMerge + '-span'] && data[index][firstMerge + '-span'].rowspan === 1)) {
          v[m + '-span'] = {
            rowspan: 1,
            colspan: 1
          }
        } else {
          data[mList[rowVal]['index']][m + '-span'].rowspan++
          v[m + '-span'] = {
            rowspan: 0,
            colspan: 0
          }
          mList[rowVal]['num']++
          mList[rowVal]['newIndex']++
        }
      } else {
        mList[rowVal] = { num: 1, index: index, newIndex: index + 1 }
        v[m + '-span'] = {
          rowspan: 1,
          colspan: 1
        }
      }
      return v
    })
  })
  return data
}
