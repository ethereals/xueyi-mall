-- ----------------------------
-- 1、保险公司信息表
-- ----------------------------
drop table if exists xy_insurance_company;
create table xy_insurance_company (
  company_id		        bigint	            not null                                comment '公司Id',
  code		                varchar(50)	        not null default ''	                    comment '公司编码',
  name		                varchar(50)	        not null	                            comment '公司名称',
  str_parameter             text	    	        	                                comment '公司参数',
  open_time                 datetime            default null                            comment '开通时间',
  close_time                datetime            default null                            comment '到期时间',
  type		                char(1)	            not null default '0'	                comment '公司类型',
  status                    char(1)             not null default '0'                    comment '状态（0正常 1停用）',
  create_by                 bigint              default null                            comment '创建者',
  create_time               datetime            default current_timestamp               comment '创建时间',
  update_by                 bigint              default null                            comment '更新者',
  update_time               datetime            on update current_timestamp             comment '更新时间',
  remark                    varchar(1000)       default null                            comment '备注',
  del_flag		            tinyint             not null default 0                      comment '删除标志（0正常 1删除）',
  system_id		            bigint	            not null                                comment '系统Id',
  tenant_id		            bigint	            not null                                comment '租户Id（0默认系统 otherId特定租户专属）',
  primary key (company_id)
) engine=innodb comment = '保险公司信息表';

-- ----------------------------
-- 2、合同信息表
-- ----------------------------
drop table if exists xy_insurance_contract;
create table xy_insurance_contract (
  contract_id		        bigint	            not null                                comment '合同Id',
  code		                varchar(50)	        not null default ''	                    comment '合同编码',
  name		                varchar(50)	        not null	                            comment '合同名称',
  str_parameter             text	    	        	                                comment '合同参数',
  open_time                 datetime            default null                            comment '开通时间',
  close_time                datetime            default null                            comment '到期时间',
  type		                char(1)	            not null default '0'	                comment '合同类型（0车险 1非车险 2寿险）',
  status                    char(1)             not null default '0'                    comment '状态（0正常 1停用）',
  create_by                 bigint              default null                            comment '创建者',
  create_time               datetime            default current_timestamp               comment '创建时间',
  update_by                 bigint              default null                            comment '更新者',
  update_time               datetime            on update current_timestamp             comment '更新时间',
  remark                    varchar(1000)       default null                            comment '备注',
  del_flag		            tinyint             not null default 0                      comment '删除标志（0正常 1删除）',
  system_id		            bigint	            not null                                comment '系统Id',
  tenant_id		            bigint	            not null                                comment '租户Id（0默认系统 otherId特定租户专属）',
  primary key (contract_id)
) engine=innodb comment = '合同信息表';

-- ----------------------------
-- 2、保单信息表
-- ----------------------------
drop table if exists xy_insurance_policy;
create table xy_insurance_policy (
  policy_id		            bigint	            not null                                comment '保单Id',
  main_order                varchar(50)	        not null default ''	                    comment '主保单号',
  sub_order                 varchar(50)	        default null	                        comment '子保单号',
  holder                    varchar(50)	        default null	                        comment '投保人',
  holder_tel                varchar(50)	        default null	                        comment '投保人手机号',
  holder_identity           varchar(50)	        default null	                        comment '投保人身份证号',
  insured                   varchar(50)	        default null	                        comment '受保人',
  insured_tel               varchar(50)	        default null	                        comment '受保人手机号',
  insured_identity          varchar(50)	        default null	                        comment '受保人身份证号',
  str_parameter             text	    	        	                                comment '合同参数',
  commission                decimal(12,2)       not null default 0                      comment '保费',
  commission_rate           decimal(12,4)       not null default 0                      comment '佣金比例',
  commission_amount         decimal(12,4)       not null default 0                      comment '佣金额',
  date_time                 datetime            default null                            comment '出单时间',
  open_time                 datetime            default null                            comment '开通时间',
  close_time                datetime            default null                            comment '到期时间',
  type		                char(1)	            not null default '0'	                comment '保单类型（0车险 1非车险 2寿险）',
  status                    char(1)             not null default '0'                    comment '状态（0正常 1停用）',
  create_by                 bigint              default null                            comment '创建者',
  create_time               datetime            default current_timestamp               comment '创建时间',
  update_by                 bigint              default null                            comment '更新者',
  update_time               datetime            on update current_timestamp             comment '更新时间',
  remark                    varchar(1000)       default null                            comment '备注',
  del_flag		            tinyint             not null default 0                      comment '删除标志（0正常 1删除）',
  system_id		            bigint	            not null                                comment '系统Id',
  tenant_id		            bigint	            not null                                comment '租户Id（0默认系统 otherId特定租户专属）',
  primary key (policy_id)
) engine=innodb comment = '保单信息表';
