
-- ----------------------------
-- 系统 | 菜单配置引入
-- ----------------------------
insert into xy_system (system_id, system_name, image_url, type, route, remark, tenant_id)
values (1 , '商城' ,  '[{"materialId": "1384755423424516096", "materialUrl": "http://127.0.0.1:9300/statics/2021/04/21/5ec82664-b6cd-48b6-92e5-478d16b61428.jpg", "materialNick": "5ec82664-b6cd-48b6-92e5-478d16b61428.jpg", "hiddenVisible": false, "materialOriginalUrl": "http://127.0.0.1:9300/statics/2021/04/21/d90c13a0-11b5-4314-ad20-f05c6ff18497.jpg"}]', '1', 'http://localhost:81/', '轻松打造在线商城', 0);

insert into sys_menu (menu_id, system_id, tenant_id, parent_id, menu_name, path, component, menu_type, visible, perms, icon, sort, create_by, remark)
values
       -- 一级菜单
       (110000, 1, 0, 0, '商城', 'setting',   null, 'M', '0', '', 'xy_organization', 1, 0, '商城目录'),
              -- 二级菜单
              (111000, 1, 0, 110000, '功能设置', 'setting',     'mall/setting/setting/index',           'C', '0',  'mall:setting:setting',          'xy_dept',   1, 0, '功能设置菜单'),
              (112000, 1, 0, 110000, '支付设置', 'payment',     'mall/setting/payment/index',           'C', '0',  'mall:setting:payment',          'xy_dept',   1, 0, '支付设置菜单'),
              (113000, 1, 0, 110000, '配送设置', 'delivery',    'mall/setting/delivery/index',          'C', '0',  'mall:setting:delivery',         'xy_dept',   1, 0, '配送设置菜单'),
              (114000, 1, 0, 110000, '门店设置', 'store',       'mall/setting/store/index',             'C', '0',  'mall:setting:store',            'xy_dept',   1, 0, '门店设置菜单'),
              (115000, 1, 0, 110000, '经营设置', 'management',  'mall/setting/management/index',        'C', '0',  'mall:setting:management',       'xy_dept',   1, 0, '经营设置菜单'),
              (116000, 1, 0, 110000, '订单设置', 'order',       'mall/setting/order/index',             'C', '0',  'mall:setting:order',            'xy_dept',   1, 0, '订单设置菜单'),
#                                    -- 部门管理按钮
#                                    (111001, 0, 0, 111000, '部门查询', '', '', 'F', '0', 'system:dept:query',           '#', 1, 0, ''),
#                                    (111002, 0, 0, 111000, '部门新增', '', '', 'F', '0', 'system:dept:add',             '#', 2, 0, ''),
#                                    (111003, 0, 0, 111000, '部门修改', '', '', 'F', '0', 'system:dept:edit',            '#', 3, 0, ''),
#                                    (111004, 0, 0, 111000, '部门删除', '', '', 'F', '0', 'system:dept:remove',          '#', 4, 0, ''),
       -- 一级菜单
       (120000, 1, 0, 0, '产品', 'product',   null, 'M', '0', '', 'xy_organization', 1, 0, '产品目录'),
              -- 二级菜单
              (121000, 1, 0, 120000, '功能设置', 'setting',     'mall/product/setting/index',           'C', '0',  'mall:setting:list',             'xy_mall_setting',      1, 0, '功能设置菜单'),
              (122000, 1, 0, 120000, '管理产品', 'product',     'mall/product/product/index',           'C', '0',  'mall:product:list',             'xy_mall_product',      1, 0, '管理产品菜单'),
                                   -- 按钮 SQL
                                   (122001, 0, 0, 122000, '产品查询', '', '', 'F', '0', 'product:product:query',           '#', 1, 0, ''),
                                   (122002, 0, 0, 122000, '产品新增', '', '', 'F', '0', 'product:product:add',             '#', 2, 0, ''),
                                   (122003, 0, 0, 122000, '产品修改', '', '', 'F', '0', 'product:product:edit',            '#', 3, 0, ''),
                                   (122004, 0, 0, 122000, '产品删除', '', '', 'F', '0', 'product:product:remove',          '#', 4, 0, ''),
                                   (122005, 0, 0, 122000, '产品导出', '', '', 'F', '0', 'product:product:export',          '#', 5, 0, ''),
              (123000, 1, 0, 120000, '产品分库', 'library',     'mall/product/library/index',           'C', '0',  'mall:library:list',             'xy_mall_library',      1, 0, '产品分库菜单'),
                                   -- 产品分库按钮
                                   (123001, 1, 0, 123000, '库查询', '', '', 'F', '0', 'product:library:query',           '#', 1, 0, ''),
                                   (123002, 1, 0, 123000, '库新增', '', '', 'F', '0', 'product:library:add',             '#', 2, 0, ''),
                                   (123003, 1, 0, 123000, '库修改', '', '', 'F', '0', 'product:library:edit',            '#', 3, 0, ''),
                                   (123004, 1, 0, 123000, '库删除', '', '', 'F', '0', 'product:library:remove',          '#', 4, 0, ''),
#                                    (123005, 0, 0, 123000, '产品库导出', '', '', 'F', '0', 'product:library:export',          '#', 5, 0, ''),
              (124000, 1, 0, 120000, '产品规格', 'spec',        'mall/product/spec/index',              'C', '0',  'mall:spec:list',                'xy_mall_spec',         1, 0, '产品规格菜单'),
                                   -- 产品规格按钮
                                   (124001, 1, 0, 124000, '规格查询', '', '', 'F', '0', 'product:spec:query',           '#', 1, 0, ''),
                                   (124002, 1, 0, 124000, '规格新增', '', '', 'F', '0', 'product:spec:add',             '#', 2, 0, ''),
                                   (124003, 1, 0, 124000, '规格修改', '', '', 'F', '0', 'product:spec:edit',            '#', 3, 0, ''),
                                   (124004, 1, 0, 124000, '规格删除', '', '', 'F', '0', 'product:spec:remove',          '#', 4, 0, ''),
#                                    (124005, 1, 0, 124000, '产品规格导出', '', '', 'F', '0', 'product:spec:export',          '#', 5, 0, ''),
              (125000, 1, 0, 120000, '产品参数', 'parameter',   'mall/product/parameter/index',         'C', '0',  'mall:parameter:list',           'xy_mall_parameter',    1, 0, '产品参数菜单'),
                                   -- 产品参数按钮
                                   (125001, 1, 0, 125000, '参数查询', '', '', 'F', '0', 'product:parameter:query',           '#', 1, 0, ''),
                                   (125002, 1, 0, 125000, '参数新增', '', '', 'F', '0', 'product:parameter:add',             '#', 2, 0, ''),
                                   (125003, 1, 0, 125000, '参数修改', '', '', 'F', '0', 'product:parameter:edit',            '#', 3, 0, ''),
                                   (125004, 1, 0, 125000, '参数删除', '', '', 'F', '0', 'product:parameter:remove',          '#', 4, 0, ''),
#                                    (125005, 1, 0, 125000, '产品参数导出', '', '', 'F', '0', 'product:parameter:export',          '#', 5, 0, ''),
              (126000, 1, 0, 120000, '产品分类', 'category',    'mall/product/category/index',          'C', '0',  'mall:category:list',            'xy_mall_category',     1, 0, '产品分类菜单'),
                                   -- 产品分类按钮
                                   (126001, 1, 0, 126000, '分类查询', '', '', 'F', '0', 'product:category:query',           '#', 1, 0, ''),
                                   (126002, 1, 0, 126000, '分类新增', '', '', 'F', '0', 'product:category:add',             '#', 2, 0, ''),
                                   (126003, 1, 0, 126000, '分类修改', '', '', 'F', '0', 'product:category:edit',            '#', 3, 0, ''),
                                   (126004, 1, 0, 126000, '分类删除', '', '', 'F', '0', 'product:category:remove',          '#', 4, 0, ''),
              (127000, 1, 0, 120000, '产品标签', 'tag',         'mall/product/tag/index',               'C', '0',  'mall:tag:list',                 'xy_mall_tag',          1, 0, '产品标签菜单'),
                                   -- 产品标签按钮
                                   (127001, 1, 0, 127000, '标签查询', '', '', 'F', '0', 'product:tag:query',           '#', 1, 0, ''),
                                   (127002, 1, 0, 127000, '标签新增', '', '', 'F', '0', 'product:tag:add',             '#', 2, 0, ''),
                                   (127003, 1, 0, 127000, '标签修改', '', '', 'F', '0', 'product:tag:edit',            '#', 3, 0, ''),
                                   (127004, 1, 0, 127000, '标签删除', '', '', 'F', '0', 'product:tag:remove',          '#', 4, 0, ''),
#                                    (127005, 1, 0, 127000, '标签导出', '', '', 'F', '0', 'product:tag:export',          '#', 5, 0, ''),
              (128000, 1, 0, 120000, '产品评论', 'comment',     'mall/product/comment/index',           'C', '0',  'mall:comment:list',             'xy_mall_comment',      1, 0, '产品评论菜单'),
       -- 一级菜单
       (130000, 1, 0, 0, '订单', 'order',     null, 'M', '0', '', 'xy_organization', 1, 0, '订单目录'),
              -- 二级菜单
              (131000, 1, 0, 130000, '功能设置', 'setting',     'mall/order/setting/index',             'C', '0',  'mall:dept:list',      'xy_dept',   1, 0, '功能设置菜单'),
              (132000, 1, 0, 130000, '管理订单', 'order',       'mall/order/order/index',               'C', '0',  'mall:dept:list',      'xy_dept',   1, 0, '管理订单菜单'),
              (133000, 1, 0, 130000, '配套工具', 'tools',       '',                                     'M', '0',  '',                    'xy_log',    4, 0, '配套工具菜单'),
                     -- 三级菜单
                     (133100, 1, 0, 133000, '商家助手', 'mallHelper',           'mall/order/tools/mallHelper/index',            'C', '0', 'mall:operlog:list',   'xy_log_operation',  1, 0, '商家助手菜单'),
                                   -- 操作日志按钮
#                                    (133101, 1, 0, 133100, '操作查询', '#', '', 'F', '0', 'mall:operlog:query',       '#', 1, 0, ''),
#                                    (133102, 1, 0, 133100, '操作删除', '#', '', 'F', '0', 'mall:operlog:remove',      '#', 2, 0, ''),
#                                    (133103, 1, 0, 133100, '日志导出', '#', '', 'F', '0', 'mall:operlog:export',      '#', 3, 0, ''),
                     -- 三级菜单
                     (133200, 1, 0, 133000, '快速打单', 'quickTicketPrinting',  'mall/order/tools/quickTicketPrinting/index',   'C', '0', 'mall:loginInfo:list', 'xy_log_loginInfo',  2, 0, '快速打单菜单'),
                                   -- 登录日志按钮
#                                    (133201, 1, 0, 133200, '登录查询', '#', '', 'F', '0', 'mall:loginInfo:query',     '#', 1, 0, ''),
#                                    (133202, 1, 0, 133200, '登录删除', '#', '', 'F', '0', 'mall:loginInfo:remove',    '#', 2, 0, ''),
#                                    (133203, 1, 0, 133200, '日志导出', '#', '', 'F', '0', 'mall:loginInfo:export',    '#', 3, 0, ''),
                     -- 三级菜单
                     (133300, 1, 0, 133000, '小票打印', 'smallTicketPrinting',  'mall/order/tools/smallTicketPrinting/index',   'C', '0', 'mall:loginInfo:list', 'xy_log_loginInfo',  3, 0, '小票打印菜单');
                                   -- 登录日志按钮
#                                    (133301, 1, 0, 133300, '登录查询', '#', '', 'F', '0', 'mall:loginInfo:query',     '#', 1, 0, ''),
#                                    (133302, 1, 0, 133300, '登录删除', '#', '', 'F', '0', 'mall:loginInfo:remove',    '#', 2, 0, ''),
#                                    (133303, 1, 0, 133300, '日志导出', '#', '', 'F', '0', 'mall:loginInfo:export',    '#', 3, 0, '');
-- ----------------------------
-- 1、商城模块表|系统1-商城n
-- ----------------------------
drop table if exists xy_system_mall;
create table xy_system_mall (
  site_id		            bigint	            not null                                comment '站点Id',
  code		                varchar(50)	        not null default ''	                    comment '商城编码',
  name		                varchar(50)	        not null	                            comment '商城名称',
  logo                      varchar(1000)	    not null default ''	        	        comment 'logo',
  main_status		        char(1)	            not null default '1'                    comment '主商城（0是 1否）',
  type		                char(1)	            not null default '0'	                comment '商城类型（0一站一店版 1一站多店版 2多站一店版 3多站多店版 4多商户版）',
  edition		            char(1)	            not null default '0'	                comment '商城版本（0入门版 1标准版 2企业版 3旗舰版）',
  open_time                 datetime            default null                            comment '开通时间',
  close_time                datetime            default null                            comment '到期时间',
  status                    char(1)             not null default '0'                    comment '状态（0正常 1停用）',
  create_by                 bigint              default null                            comment '创建者',
  create_time               datetime            default current_timestamp               comment '创建时间',
  update_by                 bigint              default null                            comment '更新者',
  update_time               datetime            on update current_timestamp             comment '更新时间',
  remark                    varchar(1000)       default null                            comment '备注',
  del_flag		            tinyint             not null default 0                      comment '删除标志（0正常 1删除）',
  system_id		            bigint	            not null                                comment '系统Id',
  tenant_id		            bigint	            not null                                comment '租户Id（0默认系统 otherId特定租户专属）',
  primary key (site_id)
) engine=innodb comment = '商城模块表';

# ----------------------------
# 初始化-商城模块表数据
# ----------------------------
insert into xy_system_mall (site_id, code, name, logo, main_status, system_id, tenant_id)
values (1, 'TSM0001', '雪忆mall',  '', '0', 1, 1),
       (2, 'TSM0002', '雪忆mall2', '', '1', 1, 1),
       (3, 'TSM0003', '雪忆mall3', '', '1', 1, 2);

-- ----------------------------
-- 2、商城配置表
-- ----------------------------
drop table if exists xy_system_mall_set;
create table xy_system_mall_set (
  mall_set_id		        bigint	            not null                                comment '配置Id',
  name		                varchar(100)	    not null	                            comment '域名名称',
  IPv6_status		        char(1)	            not null default '1'	                comment 'IPv6状态（0启用 1未启用）',
  https_status		        char(1)	            not null default '1'	                comment 'https状态（0启用 1未启用）',
  https_jump_status		    char(1)	            not null default '1'	                comment '自动跳转https（0启用 1未启用）',
  https_close_time          datetime            default null                            comment 'https到期时间',
  type		                char(1)	            not null default '0'	                comment '域名类型（0非主域名 1主域名）',
  status                    char(1)             not null default '0'                    comment '状态（0正常 1停用）',
  create_by                 bigint              default null                            comment '创建者',
  create_time               datetime            default current_timestamp               comment '创建时间',
  update_by                 bigint              default null                            comment '更新者',
  update_time               datetime            on update current_timestamp             comment '更新时间',
  del_flag		            tinyint             not null default 0                      comment '删除标志（0正常 1删除）',
  site_id		            bigint	            not null                                comment '站点Id',
  system_id		            bigint	            not null                                comment '系统Id',
  tenant_id		            bigint	            not null                                comment '租户Id（0默认系统 otherId特定租户专属）',
  primary key (mall_set_id)
) engine=innodb comment = '商城配置表';

# ----------------------------
# 初始化-商城配置表数据
# ----------------------------

-- ----------------------------
-- 3、产品库表
-- ----------------------------
drop table if exists xy_mall_library;
create table xy_mall_library (
  library_id		        bigint	            not null                                comment '产品库Id',
  name		                varchar(100)	    not null	                            comment '产品库名称',
  type		                char(1)	            not null default '0'	                comment '类型（0非系统默认 1系统默认）',
  sort                      tinyint             not null default -128                   comment '显示顺序',
  status                    char(1)             not null default '0'                    comment '状态（0正常 1停用）',
  create_by                 bigint              default null                            comment '创建者',
  create_time               datetime            default current_timestamp               comment '创建时间',
  update_by                 bigint              default null                            comment '更新者',
  update_time               datetime            on update current_timestamp             comment '更新时间',
  remark                    varchar(1000)       default null                            comment '备注',
  del_flag		            tinyint             not null default 0                      comment '删除标志（0正常 1删除）',
  site_id		            bigint	            not null                                comment '站点Id',
  system_id		            bigint	            not null                                comment '系统Id',
  tenant_id		            bigint	            not null                                comment '租户Id（0默认系统 otherId特定租户专属）',
  primary key (library_id)
) engine=innodb comment = '产品库表';

# ----------------------------
# 初始化-产品库表数据
# ----------------------------

insert into xy_mall_library (library_id, name, type, site_id, system_id, tenant_id)
values (1, '默认产品库', '1', 1, 1, 1),
       (2, '默认产品库', '1', 2, 1, 1);

-- ----------------------------
-- 4、产品参数表
-- ----------------------------
drop table if exists xy_mall_parameter;
create table xy_mall_parameter (
  parameter_id		        bigint	            not null                                comment '参数Id',
  name		                varchar(100)	    not null	                            comment '参数名称',
  view_type                 char(1)             not null default '0'                    comment '展示状态（0所有人可见 1仅登录可见 2仅会员可见）',
  membership_level          tinyint             default null                            comment '会员级别(当type=2时生效)',
  field_type		        char(1)	            not null default '0'	                comment '字段类型（0选择型 1文字输入型 2数值输入型）',
  input_type		        char(1)	            not null default '0'	                comment '填写类型（0非必填型 1必填型）',
  type		                char(1)	            not null default '0'	                comment '类型（0非系统默认 1系统默认）',
  sort                      tinyint             not null default -128                   comment '显示顺序',
  view_status               char(1)             not null default '0'                    comment '显示状态（0正常  1仅后台可见）',
  status                    char(1)             not null default '0'                    comment '状态（0正常  1停用）',
  create_by                 bigint              default null                            comment '创建者',
  create_time               datetime            default current_timestamp               comment '创建时间',
  update_by                 bigint              default null                            comment '更新者',
  update_time               datetime            on update current_timestamp             comment '更新时间',
  del_flag		            tinyint             not null default 0                      comment '删除标志（0正常 1删除）',
  library_id		        bigint	            not null                                comment '产品库Id',
  site_id		            bigint	            not null                                comment '站点Id',
  system_id		            bigint	            not null                                comment '系统Id',
  tenant_id		            bigint	            not null                                comment '租户Id（0默认系统 otherId特定租户专属）',
  primary key (parameter_id)
) engine=innodb comment = '产品参数表';

# ----------------------------
# 初始化-产品参数表数据
# ----------------------------

-- ----------------------------
-- 5、产品参数值表
-- ----------------------------
drop table if exists xy_mall_parameter_value;
create table xy_mall_parameter_value (
  parameter_value_id		bigint	            not null auto_increment                 comment '参数值Id',
  name		                varchar(100)	    not null	                            comment '参数值名称',
  sort                      tinyint             not null default -128                   comment '显示顺序',
  create_by                 bigint              default null                            comment '创建者',
  create_time               datetime            default current_timestamp               comment '创建时间',
  update_by                 bigint              default null                            comment '更新者',
  update_time               datetime            on update current_timestamp             comment '更新时间',
  del_flag		            tinyint             not null default 0                      comment '删除标志（0正常 1删除）',
  parameter_id		        bigint	            not null                                comment '参数Id',
  library_id		        bigint	            not null                                comment '产品库Id',
  site_id		            bigint	            not null                                comment '站点Id',
  system_id		            bigint	            not null                                comment '系统Id',
  tenant_id		            bigint	            not null                                comment '租户Id（0默认系统 otherId特定租户专属）',
  primary key (parameter_value_id)
) engine=innodb auto_increment=100 comment = '产品参数值表';

-- ----------------------------
-- 6、产品规格表
-- ----------------------------
drop table if exists xy_mall_spec;
create table xy_mall_spec (
  spec_id		            bigint	            not null                                comment '规格Id',
  name		                varchar(100)	    not null	                            comment '规格名称',
  sort                      tinyint             not null default -128                   comment '显示顺序',
  status                    char(1)             not null default '0'                    comment '状态（0正常 1停用）',
  create_by                 bigint              default null                            comment '创建者',
  create_time               datetime            default current_timestamp               comment '创建时间',
  update_by                 bigint              default null                            comment '更新者',
  update_time               datetime            on update current_timestamp             comment '更新时间',
  del_flag		            tinyint             not null default 0                      comment '删除标志（0正常 1删除）',
  library_id		        bigint	            not null                                comment '产品库Id',
  site_id		            bigint	            not null                                comment '站点Id',
  system_id		            bigint	            not null                                comment '系统Id',
  tenant_id		            bigint	            not null                                comment '租户Id（0默认系统 otherId特定租户专属）',
  primary key (spec_id)
) engine=innodb comment = '产品规格表';

-- ----------------------------
-- 7、产品规格值表
-- ----------------------------
drop table if exists xy_mall_spec_value;
create table xy_mall_spec_value (
  spec_value_id		        bigint	            not null auto_increment                 comment '规格值Id',
  name		                varchar(100)	    not null	                            comment '规格值名称',
  image_url                 varchar(3000)	    default ''       	        	        comment '图片',
  sort                      tinyint             not null default -128                   comment '显示顺序',
  create_by                 bigint              default null                            comment '创建者',
  create_time               datetime            default current_timestamp               comment '创建时间',
  update_by                 bigint              default null                            comment '更新者',
  update_time               datetime            on update current_timestamp             comment '更新时间',
  del_flag		            tinyint             not null default 0                      comment '删除标志（0正常 1删除）',
  spec_id		            bigint	            not null                                comment '规格Id',
  library_id		        bigint	            not null                                comment '产品库Id',
  site_id		            bigint	            not null                                comment '站点Id',
  system_id		            bigint	            not null                                comment '系统Id',
  tenant_id		            bigint	            not null                                comment '租户Id（0默认系统 otherId特定租户专属）',
  primary key (spec_value_id)
) engine=innodb auto_increment=100 comment = '产品规格值表';

-- ----------------------------
-- 8、产品分类表
-- ----------------------------
drop table if exists xy_mall_category;
create table xy_mall_category (
  category_id		        bigint	            not null                                comment '分类Id',
  name		                varchar(100)	    not null	                            comment '分类名称',
  parent_id		            bigint	            not null default 0                      comment '父分类Id',
  ancestors                 varchar(500)        default '0'                             comment '祖级列表',
  level                     tinyint             default 1                               comment '层级',
  sort                      tinyint             not null default -128                   comment '显示顺序',
  create_by                 bigint              default null                            comment '创建者',
  create_time               datetime            default current_timestamp               comment '创建时间',
  update_by                 bigint              default null                            comment '更新者',
  update_time               datetime            on update current_timestamp             comment '更新时间',
  del_flag		            tinyint             not null default 0                      comment '删除标志（0正常 1删除）',
  site_id		            bigint	            not null                                comment '站点Id',
  system_id		            bigint	            not null                                comment '系统Id',
  tenant_id		            bigint	            not null                                comment '租户Id（0默认系统 otherId特定租户专属）',
  primary key (category_id)
) engine=innodb comment = '产品分类表';

-- ----------------------------
-- 9、产品标签表
-- ----------------------------
drop table if exists xy_mall_tag;
create table xy_mall_tag (
  tag_id		            bigint	            not null                                comment '标签Id',
  name		                varchar(100)	    not null	                            comment '标签名称',
  sort                      tinyint             not null default -128                   comment '显示顺序',
  status                    char(1)             not null default '0'                    comment '状态（0正常 1停用）',
  create_by                 bigint              default null                            comment '创建者',
  create_time               datetime            default current_timestamp               comment '创建时间',
  update_by                 bigint              default null                            comment '更新者',
  update_time               datetime            on update current_timestamp             comment '更新时间',
  del_flag		            tinyint             not null default 0                      comment '删除标志（0正常 1删除）',
  site_id		            bigint	            not null                                comment '站点Id',
  system_id		            bigint	            not null                                comment '系统Id',
  tenant_id		            bigint	            not null                                comment '租户Id（0默认系统 otherId特定租户专属）',
  primary key (tag_id)
) engine=innodb comment = '产品标签表';

-- ----------------------------
-- 9、商户表
-- ----------------------------
drop table if exists xy_mall_merchant;
create table xy_mall_merchant (
  merchant_id		        bigint	            not null                                comment '商户Id',
  name		                varchar(100)	    not null	                            comment '商户名称',
  sort                      tinyint             not null default -128                   comment '显示顺序',
  status                    char(1)             not null default '0'                    comment '状态（0正常 1停用）',
  create_by                 bigint              default null                            comment '创建者',
  create_time               datetime            default current_timestamp               comment '创建时间',
  update_by                 bigint              default null                            comment '更新者',
  update_time               datetime            on update current_timestamp             comment '更新时间',
  del_flag		            tinyint             not null default 0                      comment '删除标志（0正常 1删除）',
  remark                    varchar(1000)       default null                            comment '备注',
  site_id		            bigint	            not null                                comment '站点Id',
  system_id		            bigint	            not null                                comment '系统Id',
  tenant_id		            bigint	            not null                                comment '租户Id（0默认系统 otherId特定租户专属）',
  primary key (merchant_id)
) engine=innodb comment = '商户表';

-- ----------------------------
-- 10、产品表
-- ----------------------------
drop table if exists xy_mall_product;
create table xy_mall_product (
  product_id		        bigint	            not null                                comment '产品Id',
  name		                varchar(100)	    not null	                            comment '产品名称',
  image		                varchar(5000)	    default ''	                            comment '产品图片',
  video		                varchar(500)	    default ''	                            comment '产品主图视频',
  str_parameter             text	            	                                    comment '产品参数 | [{Id:null, name:null, filedType:null, value:null, viewType:null}] | 参数Id+参数名+输入类型+参数值名+前端显示状态',
  str_spec                  text	            	                                    comment '产品规格 | {table:[{规格值名+价格+库存+重量}], prop:[{label: null, prop: null}], ids:[], specValue:[]}',
  price                     decimal(12,2) unsigned not null default 0 	                comment '价格',
  custom_price              char(1)	            not null default '0'	                comment '自定义价格（0否 1是）',
  stock                     decimal(12,0)       not null default 0 	                    comment '库存',
  custom_stock              char(1)	            not null default '0'	                comment '自定义库存（0否 1是）',
  weight                    decimal(12,4) unsigned not null default 0 	                comment '重量',
  custom_weight             char(1)	            not null default '0'	                comment '自定义重量（0否 1是）',
  sales                     int unsigned        default 0 	                            comment '销量',
  min_purchase              int unsigned        default 0 	                            comment '起购量',
  max_purchase              int unsigned        default 0 	                            comment '限购量',
  custom_purchase           char(1)	            not null default '0'	                comment '自定义起(限)购量（0否 1是）',
  type		                char(1)	            not null default '0'	                comment '产品类型（0普通商品 1卡密产品 2动价产品 3保险产品）',
  sort                      int                 not null default -128                   comment '显示顺序',
  status                    char(1)             not null default '1'                    comment '状态（0已上架 1未上架 2待上架）',
  show_time                 datetime            default null                            comment '自动上架时间（仅status=2生效，生效后自动转null）',
  create_by                 bigint              default null                            comment '创建者',
  create_time               datetime            default current_timestamp               comment '创建时间',
  update_by                 bigint              default null                            comment '更新者',
  update_time               datetime            on update current_timestamp             comment '更新时间',
  remark                    text                                                        comment '备注',
  del_flag		            tinyint             not null default 0                      comment '删除标志（0正常 1删除）',
  library_id		        bigint	            not null                                comment '产品库Id',
  site_id		            bigint	            not null                                comment '站点Id',
  system_id		            bigint	            not null                                comment '系统Id',
  tenant_id		            bigint	            not null                                comment '租户Id（0默认系统 otherId特定租户专属）',
  primary key (product_id)
) engine=innodb comment = '产品表';

-- ----------------------------
-- 11、产品和分类关联表  产品N-N分类
-- ----------------------------
drop table if exists xy_mall_product_category;
create table xy_mall_product_category (
  product_id                bigint              not null                                comment '产品Id',
  category_id               bigint              not null                                comment '分类Id',
  del_flag		            tinyint             not null default 0                      comment '删除标志（0正常 1删除）',
  library_id		        bigint	            not null                                comment '产品库Id',
  site_id		            bigint	            not null                                comment '站点Id',
  system_id		            bigint	            not null                                comment '系统Id',
  tenant_id		            bigint	            not null                                comment '租户Id（0默认系统 otherId特定租户专属）',
  primary key(product_id, category_id)
) engine=innodb comment = '产品和分类关联表';

-- ----------------------------
-- 12、产品和商户关联表  产品N-1商户
-- ----------------------------
drop table if exists xy_mall_product_merchant;
create table xy_mall_product_merchant (
  product_id                bigint              not null                                comment '产品Id',
  merchant_id               bigint              not null                                comment '商户Id',
  del_flag		            tinyint             not null default 0                      comment '删除标志（0正常 1删除）',
  library_id		        bigint	            not null                                comment '产品库Id',
  site_id		            bigint	            not null                                comment '站点Id',
  system_id		            bigint	            not null                                comment '系统Id',
  tenant_id		            bigint	            not null                                comment '租户Id（0默认系统 otherId特定租户专属）',
  primary key(product_id, merchant_id)
) engine=innodb comment = '产品和商户关联表';

-- ----------------------------
-- 12、产品和标签关联表  产品N-N标签
-- ----------------------------
drop table if exists xy_mall_product_tag;
create table xy_mall_product_tag (
  product_id                bigint              not null                                comment '产品Id',
  tag_id		            bigint	            not null                                comment '标签Id',
  del_flag		            tinyint             not null default 0                      comment '删除标志（0正常 1删除）',
  site_id		            bigint	            not null                                comment '站点Id',
  system_id		            bigint	            not null                                comment '系统Id',
  tenant_id		            bigint	            not null                                comment '租户Id（0默认系统 otherId特定租户专属）',
  primary key(product_id, tag_id)
) engine=innodb comment = '产品和标签关联表';