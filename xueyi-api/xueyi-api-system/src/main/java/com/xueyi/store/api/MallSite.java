package com.xueyi.store.api;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonFormat;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.xueyi.common.core.annotation.Excel;
import com.xueyi.common.core.web.domain.BaseEntity;

/**
 * 商城-站点模块对象 xy_system_mall
 *
 * @author xueyi
 */
public class MallSite extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 商城编码 */
    @Excel(name = "商城编码")
    private String code;

    /** 商城名称 */
    @Excel(name = "商城名称")
    private String name;

    /** logo */
    @Excel(name = "logo")
    private String logo;

    /** 主商城（0是 1否） */
    @Excel(name = "主商城", readConverterExp = "0=是,1=否")
    private String mainStatus;

    /** 商城类型（0一站一店版 1一站多店版 2多站一店版 3多站多店版 4多商户版） */
    @Excel(name = "商城类型", readConverterExp = "0=一站一店版,1=一站多店版,2=多站一店版,3=多站多店版,4=多商户版")
    private String type;

    /** 商城版本（0入门版 1标准版 2企业版 3旗舰版） */
    @Excel(name = "商城版本", readConverterExp = "0=入门版,1=标准版,2=企业版,3=旗舰版")
    private String edition;

    /** 开通时间 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "开通时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date openTime;

    /** 到期时间 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "到期时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date closeTime;

    /** 状态（0正常 1停用） */
    @Excel(name = "状态", readConverterExp = "0=正常,1=停用")
    private String status;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLogo() {
        return logo;
    }

    public void setLogo(String logo) {
        this.logo = logo;
    }

    public String getMainStatus() {
        return mainStatus;
    }

    public void setMainStatus(String mainStatus) {
        this.mainStatus = mainStatus;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getEdition() {
        return edition;
    }

    public void setEdition(String edition) {
        this.edition = edition;
    }

    public Date getOpenTime() {
        return openTime;
    }

    public void setOpenTime(Date openTime) {
        this.openTime = openTime;
    }

    public Date getCloseTime() {
        return closeTime;
    }

    public void setCloseTime(Date closeTime) {
        this.closeTime = closeTime;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
                .append("siteId", getSiteId())
                .append("code", getCode())
                .append("name", getName())
                .append("logo", getLogo())
                .append("mainStatus", getMainStatus())
                .append("type", getType())
                .append("edition", getEdition())
                .append("openTime", getOpenTime())
                .append("closeTime", getCloseTime())
                .append("status", getStatus())
                .append("createBy", getCreateBy())
                .append("createName", getCreateName())
                .append("createTime", getCreateTime())
                .append("updateBy", getUpdateBy())
                .append("updateName", getUpdateName())
                .append("updateTime", getUpdateTime())
                .append("remark", getRemark())
                .toString();
    }
}