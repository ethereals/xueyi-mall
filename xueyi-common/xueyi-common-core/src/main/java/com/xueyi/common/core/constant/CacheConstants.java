package com.xueyi.common.core.constant;

/**
 * 缓存的key 常量
 *
 * @author ruoyi
 */
public class CacheConstants
{
    /**
     * 令牌自定义标识
     */
    public static final String HEADER = "Authorization";

    /**
     * 令牌前缀
     */
    public static final String TOKEN_PREFIX = "Bearer ";

    /**
     * 权限缓存前缀
     */
    public final static String LOGIN_TOKEN_KEY = "login_tokens:";

    /**
     * 用户ID字段
     */
    public static final String DETAILS_ENTERPRISE_ID = "enterpriseId";

    /**
     * 用户ID字段
     */
    public static final String DETAILS_ENTERPRISE_NAME = "enterpriseName";

    /**
     * 用户ID字段
     */
    public static final String DETAILS_USER_ID = "userId";

    /**
     * 用户名字段
     */
    public static final String DETAILS_USERNAME = "userName";

    /**
     * 用户名字段
     */
    public static final String DETAILS_TYPE = "userType";

    /**
     * 授权信息字段
     */
    public static final String AUTHORIZATION_HEADER = "authorization";
}
