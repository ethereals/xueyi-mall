package com.xueyi.common.core.constant;

/**
 * 服务名称
 * 
 * @author ruoyi
 */
public class ServiceNameConstants
{
    /**
     * 认证服务的serviceid
     */
    public static final String AUTH_SERVICE = "xueyi-auth";

    /**
     * 系统模块的serviceid
     */
    public static final String SYSTEM_SERVICE = "xueyi-system";

    /**
     * 文件服务的serviceid
     */
    public static final String FILE_SERVICE = "xueyi-file";
}
