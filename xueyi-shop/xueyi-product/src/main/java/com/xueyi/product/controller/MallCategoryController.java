package com.xueyi.product.controller;

import java.util.List;
import java.io.IOException;
import javax.servlet.http.HttpServletResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.xueyi.common.log.annotation.Log;
import com.xueyi.common.log.enums.BusinessType;
import com.xueyi.common.security.annotation.PreAuthorize;
import com.xueyi.product.domain.MallCategory;
import com.xueyi.product.service.IMallCategoryService;
import com.xueyi.common.core.web.controller.BaseController;
import com.xueyi.common.core.web.domain.AjaxResult;
import com.xueyi.common.core.utils.poi.ExcelUtil;

/**
 * 产品分类 业务处理
 *
 * @author xueyi
 */
@RestController
@RequestMapping("/category")
public class MallCategoryController extends BaseController
{
    @Autowired
    private IMallCategoryService mallCategoryService;

    /**
     * 查询产品分类列表
     */
    @PreAuthorize(hasPermi = "product:category:list")
    @GetMapping("/list")
    public AjaxResult list(MallCategory mallCategory)
    {
        List<MallCategory> list = mallCategoryService.selectMallCategoryList(mallCategory);
        return AjaxResult.success(list);
    }

    /**
     * 查询产品分类树选项列表
     */
    @GetMapping("/tree")
    public AjaxResult tree(MallCategory mallCategory)
    {
        List<MallCategory> tree = mallCategoryService.selectMallCategoryTree(mallCategory);
        return AjaxResult.success(tree);
    }

    /**
     * 导出产品分类列表
     */
    @PreAuthorize(hasPermi = "product:category:export")
    @Log(title = "产品分类", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, MallCategory mallCategory) throws IOException
    {
        List<MallCategory> list = mallCategoryService.selectMallCategoryList(mallCategory);
        ExcelUtil<MallCategory> util = new ExcelUtil<MallCategory>(MallCategory.class);
        util.exportExcel(response, list, "产品分类数据");
    }

    /**
     * 获取产品分类详细信息
     */
    @PreAuthorize(hasPermi = "product:category:query")
    @GetMapping(value = "/byId")
    public AjaxResult getInfo(MallCategory mallCategory)
    {
        return AjaxResult.success(mallCategoryService.selectMallCategoryById(mallCategory));
    }

    /**
     * 新增产品分类
     */
    @PreAuthorize(hasPermi = "product:category:add")
    @Log(title = "产品分类", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody MallCategory mallCategory)
    {
        return toAjax(mallCategoryService.insertMallCategory(mallCategory));
    }

    /**
     * 修改产品分类
     */
    @PreAuthorize(hasPermi = "product:category:edit")
    @Log(title = "产品分类", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody MallCategory mallCategory)
    {
        return toAjax(mallCategoryService.updateMallCategory(mallCategory));
    }

    /**
     * 删除产品分类
     */
    @PreAuthorize(hasPermi = "product:category:remove")
    @Log(title = "产品分类", businessType = BusinessType.DELETE)
    @DeleteMapping
    public AjaxResult remove(@RequestBody MallCategory mallCategory)
    {
        return toAjax(mallCategoryService.deleteMallCategoryById(mallCategory));
    }
}