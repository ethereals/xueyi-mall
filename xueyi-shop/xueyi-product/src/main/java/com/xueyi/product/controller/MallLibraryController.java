package com.xueyi.product.controller;

import java.util.List;
import java.io.IOException;
import javax.servlet.http.HttpServletResponse;

import com.xueyi.product.api.MallLibrary;
import com.xueyi.product.service.IMallLibraryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.xueyi.common.log.annotation.Log;
import com.xueyi.common.log.enums.BusinessType;
import com.xueyi.common.security.annotation.PreAuthorize;
import com.xueyi.common.core.web.controller.BaseController;
import com.xueyi.common.core.web.domain.AjaxResult;
import com.xueyi.common.core.utils.poi.ExcelUtil;
import com.xueyi.common.core.web.page.TableDataInfo;

/**
 * 产品库
 *
 * @author xueyi
 */
@RestController
@RequestMapping("/library")
public class MallLibraryController extends BaseController {
    @Autowired
    private IMallLibraryService mallLibraryService;

    /**
     * 查询产品库列表
     */
    @GetMapping("/select")
    public AjaxResult select(MallLibrary mallLibrary) {
        return AjaxResult.success(mallLibraryService.selectAllList(mallLibrary));
    }

    /**
     * 查询产品库列表
     */
    @PreAuthorize(hasPermi = "product:library:list")
    @GetMapping("/list")
    public TableDataInfo list(MallLibrary mallLibrary) {
        startPage();
        List<MallLibrary> list = mallLibraryService.selectMallLibraryList(mallLibrary);
        return getDataTable(list);
    }

    /**
     * 获取产品库详细信息
     */
    @PreAuthorize(hasPermi = "product:library:query")
    @GetMapping(value = "/byId")
    public AjaxResult getInfo(MallLibrary mallLibrary) {
        return AjaxResult.success(mallLibraryService.selectMallLibraryById(mallLibrary));
    }

    /**
     * 新增产品库
     */
    @PreAuthorize(hasPermi = "product:library:add")
    @Log(title = "产品库", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody MallLibrary mallLibrary) {
        return toAjax(mallLibraryService.insertMallLibrary(mallLibrary));
    }

    /**
     * 修改产品库
     */
    @PreAuthorize(hasPermi = "product:library:edit")
    @Log(title = "产品库", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody MallLibrary mallLibrary) {
        return toAjax(mallLibraryService.updateMallLibrary(mallLibrary));
    }

    /**
     * 修改产品库顺序
     */
    @PreAuthorize(hasPermi = "product:library:edit")
    @Log(title = "产品库", businessType = BusinessType.UPDATE)
    @PutMapping("/sort")
    public AjaxResult updateSort(@RequestBody MallLibrary mallLibrary) {
        return toAjax(mallLibraryService.updateMallLibrarySort(mallLibrary));
    }


    /**
     * 删除产品库
     */
    @PreAuthorize(hasPermi = "product:library:remove")
    @Log(title = "产品库", businessType = BusinessType.DELETE)
    @DeleteMapping
    public AjaxResult remove(@RequestBody MallLibrary mallLibrary) {
        return toAjax(mallLibraryService.deleteMallLibraryByIds(mallLibrary));
    }
}
