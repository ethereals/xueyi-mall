package com.xueyi.product.controller;

import java.util.List;
import java.io.IOException;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.xueyi.common.log.annotation.Log;
import com.xueyi.common.log.enums.BusinessType;
import com.xueyi.common.security.annotation.PreAuthorize;
import com.xueyi.product.domain.MallParameter;
import com.xueyi.product.service.IMallParameterService;
import com.xueyi.common.core.web.controller.BaseController;
import com.xueyi.common.core.web.domain.AjaxResult;
import com.xueyi.common.core.utils.poi.ExcelUtil;
import com.xueyi.common.core.web.page.TableDataInfo;

/**
 * 产品参数Controller
 *
 * @author xueyi
 */
@RestController
@RequestMapping("/parameter")
public class MallParameterController extends BaseController {
    @Autowired
    private IMallParameterService mallParameterService;

    /**
     * 查询产品参数列表赋予产品
     */
    @GetMapping("/listToProduct")
    public AjaxResult listToProduct(MallParameter mallParameter) {
        return AjaxResult.success(mallParameterService.selectMallParameterListToProduct(mallParameter));
    }

    /**
     * 查询产品参数列表
     */
    @PreAuthorize(hasPermi = "product:parameter:list")
    @GetMapping("/list")
    public TableDataInfo list(MallParameter mallParameter) {
        startPage();
        List<MallParameter> list = mallParameterService.selectMallParameterList(mallParameter);
        return getDataTable(list);
    }

    /**
     * 导出产品参数列表
     */
    @PreAuthorize(hasPermi = "product:parameter:export")
    @Log(title = "产品参数", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, MallParameter mallParameter) throws IOException {
        List<MallParameter> list = mallParameterService.selectMallParameterList(mallParameter);
        ExcelUtil<MallParameter> util = new ExcelUtil<MallParameter>(MallParameter.class);
        util.exportExcel(response, list, "产品参数数据");
    }

    /**
     * 获取产品参数详细信息
     */
    @PreAuthorize(hasPermi = "product:parameter:query")
    @GetMapping(value = "/byId")
    public AjaxResult getInfo(MallParameter mallParameter) {
        return AjaxResult.success(mallParameterService.selectMallParameterById(mallParameter));
    }

    /**
     * 新增产品参数
     */
    @PreAuthorize(hasPermi = "product:parameter:add")
    @Log(title = "产品参数", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody MallParameter mallParameter) {
        return toAjax(mallParameterService.insertMallParameter(mallParameter));
    }

    /**
     * 修改产品参数
     */
    @PreAuthorize(hasPermi = "product:parameter:edit")
    @Log(title = "产品参数", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody MallParameter mallParameter) {
        return toAjax(mallParameterService.updateMallParameter(mallParameter));
    }

    /**
     * 修改产品参数顺序
     */
    @PreAuthorize(hasPermi = "product:parameter:edit")
    @Log(title = "产品参数", businessType = BusinessType.UPDATE)
    @PutMapping("/sort")
    public AjaxResult updateSort(@RequestBody MallParameter mallParameter) {
        return toAjax(mallParameterService.updateMallParameterSort(mallParameter));
    }

    /**
     * 删除产品参数
     */
    @PreAuthorize(hasPermi = "product:parameter:remove")
    @Log(title = "产品参数", businessType = BusinessType.DELETE)
    @DeleteMapping
    public AjaxResult remove(@RequestBody MallParameter mallParameter) {
        return toAjax(mallParameterService.deleteMallParameterByIds(mallParameter));
    }
}