package com.xueyi.product.controller;

import java.util.List;
import java.io.IOException;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.xueyi.common.log.annotation.Log;
import com.xueyi.common.log.enums.BusinessType;
import com.xueyi.common.security.annotation.PreAuthorize;
import com.xueyi.product.domain.MallProduct;
import com.xueyi.product.service.IMallProductService;
import com.xueyi.common.core.web.controller.BaseController;
import com.xueyi.common.core.web.domain.AjaxResult;
import com.xueyi.common.core.utils.poi.ExcelUtil;
import com.xueyi.common.core.web.page.TableDataInfo;

/**
 * 产品 业务处理
 *
 * @author xueyi
 */
@RestController
@RequestMapping("/product")
public class MallProductController extends BaseController {
    @Autowired
    private IMallProductService mallProductService;

    /**
     * 查询产品列表
     */
    @PreAuthorize(hasPermi = "product:product:list")
    @GetMapping("/list")
    public TableDataInfo list(MallProduct mallProduct) {
        startPage();
        List<MallProduct> list = mallProductService.selectMallProductList(mallProduct);
        return getDataTable(list);
    }


    /**
     * 导出产品列表
     */
    @PreAuthorize(hasPermi = "product:product:export")
    @Log(title = "产品", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, MallProduct mallProduct) throws IOException {
        List<MallProduct> list = mallProductService.selectMallProductList(mallProduct);
        ExcelUtil<MallProduct> util = new ExcelUtil<MallProduct>(MallProduct.class);
        util.exportExcel(response, list, "产品数据");
    }

    /**
     * 获取产品详细信息
     */
    @PreAuthorize(hasPermi = "product:product:query")
    @GetMapping(value = "/byId")
    public AjaxResult getInfo(MallProduct mallProduct) {
        return AjaxResult.success(mallProductService.selectMallProductById(mallProduct));
    }

    /**
     * 新增产品
     */
    @PreAuthorize(hasPermi = "product:product:add")
    @Log(title = "产品", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody MallProduct mallProduct) {
        return toAjax(mallProductService.insertMallProduct(mallProduct));
    }

    /**
     * 修改产品
     */
    @PreAuthorize(hasPermi = "product:product:edit")
    @Log(title = "产品", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody MallProduct mallProduct) {
        return toAjax(mallProductService.updateMallProduct(mallProduct));
    }

    /**
     * 修改产品排序
     */
    @PreAuthorize(hasPermi = "product:product:edit")
    @Log(title = "产品", businessType = BusinessType.UPDATE)
    @PutMapping(value = "/sort")
    public AjaxResult updateSort(@RequestBody MallProduct mallProduct) {
        return toAjax(mallProductService.updateMallProductSort(mallProduct));
    }

    /**
     * 删除产品
     */
    @PreAuthorize(hasPermi = "product:product:remove")
    @Log(title = "产品", businessType = BusinessType.DELETE)
    @DeleteMapping
    public AjaxResult remove(@RequestBody MallProduct mallProduct) {
        return toAjax(mallProductService.deleteMallProductByIds(mallProduct));
    }
}