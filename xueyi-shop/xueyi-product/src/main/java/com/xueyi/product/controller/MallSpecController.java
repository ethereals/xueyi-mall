package com.xueyi.product.controller;

import java.util.List;
import java.io.IOException;
import javax.servlet.http.HttpServletResponse;

import com.xueyi.product.domain.MallTag;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.xueyi.common.log.annotation.Log;
import com.xueyi.common.log.enums.BusinessType;
import com.xueyi.common.security.annotation.PreAuthorize;
import com.xueyi.product.domain.MallSpec;
import com.xueyi.product.service.IMallSpecService;
import com.xueyi.common.core.web.controller.BaseController;
import com.xueyi.common.core.web.domain.AjaxResult;
import com.xueyi.common.core.utils.poi.ExcelUtil;
import com.xueyi.common.core.web.page.TableDataInfo;

/**
 * 产品规格Controller
 *
 * @author xueyi
 */
@RestController
@RequestMapping("/spec")
public class MallSpecController extends BaseController {
    @Autowired
    private IMallSpecService mallSpecService;

    /**
     * 查询产品规格列表赋予产品
     */
    @GetMapping("/listToProduct")
    public AjaxResult listToProduct(MallSpec mallSpec) {
        return AjaxResult.success(mallSpecService.selectMallSpecListToProduct(mallSpec));
    }

    /**
     * 查询产品规格列表
     */
    @PreAuthorize(hasPermi = "product:spec:list")
    @GetMapping("/list")
    public TableDataInfo list(MallSpec mallSpec) {
        startPage();
        List<MallSpec> list = mallSpecService.selectMallSpecList(mallSpec);
        return getDataTable(list);
    }

    /**
     * 导出产品规格列表
     */
    @PreAuthorize(hasPermi = "product:spec:export")
    @Log(title = "产品规格", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, MallSpec mallSpec) throws IOException {
        List<MallSpec> list = mallSpecService.selectMallSpecList(mallSpec);
        ExcelUtil<MallSpec> util = new ExcelUtil<MallSpec>(MallSpec.class);
        util.exportExcel(response, list, "产品规格数据");
    }

    /**
     * 获取产品规格详细信息
     */
    @PreAuthorize(hasPermi = "product:spec:query")
    @GetMapping(value = "/byId")
    public AjaxResult getInfo(MallSpec mallSpec) {
        return AjaxResult.success(mallSpecService.selectMallSpecById(mallSpec));
    }

    /**
     * 新增产品规格
     */
    @PreAuthorize(hasPermi = "product:spec:add")
    @Log(title = "产品规格", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody MallSpec mallSpec) {
        return toAjax(mallSpecService.insertMallSpec(mallSpec));
    }

    /**
     * 修改产品规格
     */
    @PreAuthorize(hasPermi = "product:spec:edit")
    @Log(title = "产品规格", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody MallSpec mallSpec) {
        return toAjax(mallSpecService.updateMallSpec(mallSpec));
    }

    /**
     * 修改产品规格顺序
     */
    @PreAuthorize(hasPermi = "product:spec:edit")
    @Log(title = "产品规格", businessType = BusinessType.UPDATE)
    @PutMapping("/sort")
    public AjaxResult updateSort(@RequestBody MallSpec mallSpec)
    {
        return toAjax(mallSpecService.updateMallSpecSort(mallSpec));
    }

    /**
     * 删除产品规格
     */
    @PreAuthorize(hasPermi = "product:spec:remove")
    @Log(title = "产品规格", businessType = BusinessType.DELETE)
    @DeleteMapping
    public AjaxResult remove(@RequestBody MallSpec mallSpec) {
        return toAjax(mallSpecService.deleteMallSpecByIds(mallSpec));
    }
}