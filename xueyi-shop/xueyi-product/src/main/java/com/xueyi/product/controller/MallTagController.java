package com.xueyi.product.controller;

import java.util.List;
import java.io.IOException;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.xueyi.common.log.annotation.Log;
import com.xueyi.common.log.enums.BusinessType;
import com.xueyi.common.security.annotation.PreAuthorize;
import com.xueyi.product.domain.MallTag;
import com.xueyi.product.service.IMallTagService;
import com.xueyi.common.core.web.controller.BaseController;
import com.xueyi.common.core.web.domain.AjaxResult;
import com.xueyi.common.core.utils.poi.ExcelUtil;
import com.xueyi.common.core.web.page.TableDataInfo;

/**
 * 产品标签 业务处理
 *
 * @author xueyi
 */
@RestController
@RequestMapping("/tag")
public class MallTagController extends BaseController {
    @Autowired
    private IMallTagService mallTagService;

    /**
     * 查询产品标签列表
     */
    @PreAuthorize(hasPermi = "product:tag:list")
    @GetMapping("/list")
    public TableDataInfo list(MallTag mallTag) {
        startPage();
        List<MallTag> list = mallTagService.selectMallTagList(mallTag);
        return getDataTable(list);
    }


    /**
     * 导出产品标签列表
     */
    @PreAuthorize(hasPermi = "product:tag:export")
    @Log(title = "产品标签", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, MallTag mallTag) throws IOException {
        List<MallTag> list = mallTagService.selectMallTagList(mallTag);
        ExcelUtil<MallTag> util = new ExcelUtil<MallTag>(MallTag.class);
        util.exportExcel(response, list, "产品标签数据");
    }

    /**
     * 获取产品标签详细信息
     */
    @PreAuthorize(hasPermi = "product:tag:query")
    @GetMapping(value = "/byId")
    public AjaxResult getInfo(MallTag mallTag) {
        return AjaxResult.success(mallTagService.selectMallTagById(mallTag));
    }

    /**
     * 新增产品标签
     */
    @PreAuthorize(hasPermi = "product:tag:add")
    @Log(title = "产品标签", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody MallTag mallTag) {
        return toAjax(mallTagService.insertMallTag(mallTag));
    }

    /**
     * 修改产品标签
     */
    @PreAuthorize(hasPermi = "product:tag:edit")
    @Log(title = "产品标签", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody MallTag mallTag) {
        return toAjax(mallTagService.updateMallTag(mallTag));
    }

    /**
     * 修改产品标签顺序
     */
    @PreAuthorize(hasPermi = "product:tag:edit")
    @Log(title = "产品标签", businessType = BusinessType.UPDATE)
    @PutMapping("/sort")
    public AjaxResult updateSort(@RequestBody MallTag mallTag) {
        return toAjax(mallTagService.updateMallTagSort(mallTag));
    }

    /**
     * 删除产品标签
     */
    @PreAuthorize(hasPermi = "product:tag:remove")
    @Log(title = "产品标签", businessType = BusinessType.DELETE)
    @DeleteMapping
    public AjaxResult remove(@RequestBody MallTag mallTag) {
        return toAjax(mallTagService.deleteMallTagByIds(mallTag));
    }
}