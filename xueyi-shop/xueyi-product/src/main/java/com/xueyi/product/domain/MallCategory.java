package com.xueyi.product.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.xueyi.common.core.annotation.Excel;
import com.xueyi.common.core.web.domain.TreeEntity;

/**
 * 产品分类对象 xy_mall_category
 *
 * @author xueyi
 */
public class MallCategory extends TreeEntity
{
    private static final long serialVersionUID = 1L;

    /** 产品分类Id */
    private Long categoryId;

    /** 产品分类名称 */
    @Excel(name = "产品分类名称")
    private String name;

    /** 层级 */
    private Long level;

    public void setCategoryId(Long categoryId)
    {
        this.categoryId = categoryId;
    }

    public Long getCategoryId()
    {
        return categoryId;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    public String getName()
    {
        return name;
    }

    public void setLevel(Long level)
    {
        this.level = level;
    }

    public Long getLevel()
    {
        return level;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
                .append("categoryId", getCategoryId())
                .append("name", getName())
                .append("parentId", getParentId())
                .append("parentName", getParentName())
                .append("ancestors", getAncestors())
                .append("level", getLevel())
                .append("sort", getSort())
                .append("createBy", getCreateBy())
                .append("createName", getCreateName())
                .append("createTime", getCreateTime())
                .append("updateBy", getUpdateBy())
                .append("updateName", getUpdateName())
                .append("updateTime", getUpdateTime())
                .toString();
    }
}