package com.xueyi.product.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.xueyi.common.core.annotation.Excel;
import com.xueyi.common.core.web.domain.BaseEntity;

import java.util.List;

/**
 * 产品参数对象 xy_mall_parameter
 *
 * @author xueyi
 */
public class MallParameter extends BaseEntity {
    private static final long serialVersionUID = 1L;

    /** 参数Id */
    private Long parameterId;

    /** 参数名称 */
    @Excel(name = "参数名称")
    private String name;

    /** 展示状态（0所有人可见 1仅登录可见 2仅会员可见） */
    @Excel(name = "展示状态", readConverterExp = "0=所有人可见,1=仅登录可见,2=仅会员可见")
    private String viewType;

    /** 会员级别(当type=2时生效) */
    @Excel(name = "会员级别(当type=2时生效)")
    private Long membershipLevel;

    /** 字段类型（0选择型 1文字输入型 2数值输入型） */
    @Excel(name = "字段类型", readConverterExp = "0=选择型,1=文字输入型,2=数值输入型")
    private String fieldType;

    /** 字段类型（0非必填型 1必填型） */
    @Excel(name = "填写类型", readConverterExp = "0=非必填型,1=必填型")
    private String inputType;

    /** 类型（0非系统默认 1系统默认） */
    @Excel(name = "类型", readConverterExp = "0=非系统默认,1=系统默认")
    private String type;

    /** 显示状态（0正常 1仅后台可见） */
    @Excel(name = "显示状态", readConverterExp = "0=正常,1=仅后台可见")
    private String viewStatus;

    /** 状态（0正常 1仅后台可见 2停用） */
    @Excel(name = "状态", readConverterExp = "0=正常,1=停用")
    private String status;

    /** 参数值列表 */
    private List<MallParameterValue> values;

    public Long getParameterId() {
        return parameterId;
    }

    public void setParameterId(Long parameterId) {
        this.parameterId = parameterId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getViewType() {
        return viewType;
    }

    public void setViewType(String viewType) {
        this.viewType = viewType;
    }

    public Long getMembershipLevel() {
        return membershipLevel;
    }

    public void setMembershipLevel(Long membershipLevel) {
        this.membershipLevel = membershipLevel;
    }

    public String getFieldType() {
        return fieldType;
    }

    public void setFieldType(String fieldType) {
        this.fieldType = fieldType;
    }

    public String getInputType() {
        return inputType;
    }

    public void setInputType(String inputType) {
        this.inputType = inputType;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getViewStatus() {
        return viewStatus;
    }

    public void setViewStatus(String viewStatus) {
        this.viewStatus = viewStatus;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public List<MallParameterValue> getValues() {
        return values;
    }

    public void setValues(List<MallParameterValue> values) {
        this.values = values;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
                .append("parameterId", getParameterId())
                .append("name", getName())
                .append("viewType", getViewType())
                .append("membershipLevel", getMembershipLevel())
                .append("fieldType", getFieldType())
                .append("inputType", getInputType())
                .append("type", getType())
                .append("values", getValues())
                .append("sort", getSort())
                .append("viewStatus", getViewStatus())
                .append("status", getStatus())
                .append("createBy", getCreateBy())
                .append("createName", getCreateName())
                .append("createTime", getCreateTime())
                .append("updateBy", getUpdateBy())
                .append("updateName", getUpdateName())
                .append("updateTime", getUpdateTime())
                .toString();
    }
}