package com.xueyi.product.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.xueyi.common.core.web.domain.BaseEntity;

/**
 * 产品参数值对象 xy_mall_parameter_value
 *
 * @author xueyi
 */
public class MallParameterValue extends BaseEntity {
    private static final long serialVersionUID = 1L;

    /** 参数值Id */
    private Long parameterValueId;

    /** 参数值名称 */
    private String name;

    /** 参数Id */
    private Long parameterId;

    public Long getParameterValueId() {
        return parameterValueId;
    }

    public void setParameterValueId(Long parameterValueId) {
        this.parameterValueId = parameterValueId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Long getParameterId() {
        return parameterId;
    }

    public void setParameterId(Long parameterId) {
        this.parameterId = parameterId;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this, ToStringStyle.MULTI_LINE_STYLE)
                .append("parameterValueId", getParameterValueId())
                .append("name", getName())
                .append("sort", getSort())
                .append("createBy", getCreateBy())
                .append("createName", getCreateName())
                .append("createTime", getCreateTime())
                .append("updateBy", getUpdateBy())
                .append("updateName", getUpdateName())
                .append("updateTime", getUpdateTime())
                .toString();
    }
}