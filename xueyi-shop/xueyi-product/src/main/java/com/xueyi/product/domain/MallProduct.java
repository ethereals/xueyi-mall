package com.xueyi.product.domain;

import java.math.BigDecimal;
import java.util.Date;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.xueyi.common.core.annotation.Excel;
import com.xueyi.common.core.web.domain.BaseEntity;

/**
 * 产品对象 xy_mall_product
 *
 * @author xueyi
 */
public class MallProduct extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 产品Id */
    private Long productId;

    /** 产品名称 */
    @Excel(name = "产品名称")
    private String name;

    /** 产品图片 */
    @Excel(name = "产品图片")
    private String image;

    /** 产品主图视频 */
    @Excel(name = "产品主图视频")
    private String video;

    /** 产品参数 | 参数Id+参数名+输入类型+参数值名+前端显示状态 */
    @Excel(name = "产品参数")
    private String strParameter;

    /** 产品规格 | [规格Id+规格名+规格值名]+价格+库存+重量 */
    @Excel(name = "产品规格")
    private String strSpec;

    /** 价格 */
    @Excel(name = "价格")
    private BigDecimal price;

    /** 自定义价格（0否 1是） */
    @Excel(name = "自定义价格", readConverterExp = "0=否,1=是")
    private String customPrice;

    /** 库存 */
    @Excel(name = "库存")
    private Long stock;

    /** 自定义库存（0否 1是） */
    @Excel(name = "自定义库存", readConverterExp = "0=否,1=是")
    private String customStock;

    /** 重量 */
    @Excel(name = "重量")
    private BigDecimal weight;

    /** 自定义重量（0否 1是） */
    @Excel(name = "自定义重量", readConverterExp = "0=否,1=是")
    private String customWeight;

    /** 起购量 */
    @Excel(name = "起购量")
    private String minPurchase;

    /** 限购量 */
    @Excel(name = "限购量")
    private String maxPurchase;

    /** 自定义起(限)购量（0否 1是） */
    @Excel(name = "自定义起(限)购量", readConverterExp = "0=否,1=是")
    private String customPurchase;

    /** 邮费状态（0包邮 1不包邮） */
    @Excel(name = "邮费状态", readConverterExp = "0=包邮,1=不包邮")
    private String postageType;

    /** 销量 */
    @Excel(name = "销量")
    private Long sales;

    /** 产品类型（0普通商品 1卡密产品 2动价产品 3保险产品） */
    @Excel(name = "产品类型", readConverterExp = "0=普通商品,1=卡密产品,2=动价产品,3=保险产品")
    private String type;

    /** 状态（0已上架 1未上架 2待上架） */
    @Excel(name = "状态", readConverterExp = "0=已上架,1=未上架,2=待上架")
    private String status;

    /** 自动上架时间（仅status=2生效，生效后自动转null） */
    @Excel(name = "自动上架时间", readConverterExp = "仅=status=2生效，生效后自动转null")
    private Date showTime;

    public void setProductId(Long productId) {
        this.productId = productId;
    }

    public Long getProductId() {
        return productId;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getImage() {
        return image;
    }

    public void setVideo(String video) {
        this.video = video;
    }

    public String getVideo() {
        return video;
    }

    public void setStrParameter(String strParameter) {
        this.strParameter = strParameter;
    }

    public String getStrParameter() {
        return strParameter;
    }

    public void setStrSpec(String strSpec) {
        this.strSpec = strSpec;
    }

    public String getStrSpec() {
        return strSpec;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setCustomPrice(String customPrice) {
        this.customPrice = customPrice;
    }

    public String getCustomPrice() {
        return customPrice;
    }

    public void setStock(Long stock) {
        this.stock = stock;
    }

    public Long getStock() {
        return stock;
    }

    public void setCustomStock(String customStock) {
        this.customStock = customStock;
    }

    public String getCustomStock() {
        return customStock;
    }

    public void setWeight(BigDecimal weight) {
        this.weight = weight;
    }

    public BigDecimal getWeight() {
        return weight;
    }

    public void setCustomWeight(String customWeight) {
        this.customWeight = customWeight;
    }

    public String getCustomWeight() {
        return customWeight;
    }

    public String getMinPurchase() {
        return minPurchase;
    }

    public void setMinPurchase(String minPurchase) {
        this.minPurchase = minPurchase;
    }

    public String getMaxPurchase() {
        return maxPurchase;
    }

    public void setMaxPurchase(String maxPurchase) {
        this.maxPurchase = maxPurchase;
    }

    public String getCustomPurchase() {
        return customPurchase;
    }

    public void setCustomPurchase(String customPurchase) {
        this.customPurchase = customPurchase;
    }

    public String getPostageType() {
        return postageType;
    }

    public void setPostageType(String postageType) {
        this.postageType = postageType;
    }

    public Long getSales() {
        return sales;
    }

    public void setSales(Long sales) {
        this.sales = sales;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getType() {
        return type;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getStatus() {
        return status;
    }

    public void setShowTime(Date showTime) {
        this.showTime = showTime;
    }

    public Date getShowTime() {
        return showTime;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
                .append("productId", getProductId())
                .append("name", getName())
                .append("image", getImage())
                .append("video", getVideo())
                .append("strParameter", getStrParameter())
                .append("strSpec", getStrSpec())
                .append("price", getPrice())
                .append("customPrice", getCustomPrice())
                .append("stock", getStock())
                .append("customStock", getCustomStock())
                .append("weight", getWeight())
                .append("customWeight", getCustomWeight())
                .append("minPurchase", getMinPurchase())
                .append("maxPurchase", getMaxPurchase())
                .append("customPurchase", getCustomPurchase())
                .append("postageType", getPostageType())
                .append("sales", getSales())
                .append("type", getType())
                .append("sort", getSort())
                .append("status", getStatus())
                .append("showTime", getShowTime())
                .append("createBy", getCreateBy())
                .append("createName", getCreateName())
                .append("createTime", getCreateTime())
                .append("updateBy", getUpdateBy())
                .append("updateName", getUpdateName())
                .append("updateTime", getUpdateTime())
                .append("remark", getRemark())
                .toString();
    }
}