package com.xueyi.product.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.xueyi.common.core.annotation.Excel;
import com.xueyi.common.core.web.domain.BaseEntity;

import java.util.List;

/**
 * 产品规格对象 xy_mall_spec
 *
 * @author xueyi
 */
public class MallSpec extends BaseEntity {
    private static final long serialVersionUID = 1L;

    /**规格Id*/
    private Long specId;

    /**规格名称*/
    @Excel(name = "规格名称")
    private String name;

    /**状态（0正常 1停用）*/
    @Excel(name = "状态", readConverterExp = "0=正常,1=停用")
    private String status;

    /** 规格值列表 */
    private List<MallSpecValue> values;

    public Long getSpecId() {
        return specId;
    }

    public void setSpecId(Long specId) {
        this.specId = specId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public List<MallSpecValue> getValues() {
        return values;
    }

    public void setValues(List<MallSpecValue> values) {
        this.values = values;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this, ToStringStyle.MULTI_LINE_STYLE)
                .append("specId", getSpecId())
                .append("name", getName())
                .append("values", getValues())
                .append("sort", getSort())
                .append("status", getStatus())
                .append("createBy", getCreateBy())
                .append("createName", getCreateName())
                .append("createTime", getCreateTime())
                .append("updateBy", getUpdateBy())
                .append("updateName", getUpdateName())
                .append("updateTime", getUpdateTime())
                .toString();
    }
}