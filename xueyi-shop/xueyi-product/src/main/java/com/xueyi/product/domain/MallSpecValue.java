package com.xueyi.product.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.xueyi.common.core.web.domain.BaseEntity;

/**
 * 产品规格值对象 xy_mall_spec_value
 *
 * @author xueyi
 */
public class MallSpecValue extends BaseEntity {
    private static final long serialVersionUID = 1L;

    /** 规格值Id */
    private Long specValueId;

    /** 规格值名称 */
    private String name;

    /** 图片 */
    private String imageUrl;

    /** 规格Id */
    private Long specId;

    public Long getSpecValueId() {
        return specValueId;
    }

    public void setSpecValueId(Long specValueId) {
        this.specValueId = specValueId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public Long getSpecId() {
        return specId;
    }

    public void setSpecId(Long specId) {
        this.specId = specId;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this, ToStringStyle.MULTI_LINE_STYLE)
                .append("specValueId", getSpecValueId())
                .append("name", getName())
                .append("imageUrl", getImageUrl())
                .append("sort", getSort())
                .append("createBy", getCreateBy())
                .append("createName", getCreateName())
                .append("createTime", getCreateTime())
                .append("updateBy", getUpdateBy())
                .append("updateName", getUpdateName())
                .append("updateTime", getUpdateTime())
                .toString();
    }
}