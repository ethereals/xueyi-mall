package com.xueyi.product.mapper;

import java.util.List;

import com.xueyi.common.datascope.annotation.DataScope;
import com.xueyi.product.domain.MallCategory;

/**
 * 产品分类 数据层
 *
 * @author xueyi
 */
public interface MallCategoryMapper {
    /**
     * 查询产品分类列表
     * 访问控制 e 租户查询
     *
     * @param mallCategory 产品分类
     * @return 产品分类集合
     */
    @DataScope(SWeAlias = "e")
    public List<MallCategory> selectMallCategoryList(MallCategory mallCategory);

    /**
     * 查询产品分类树选项列表
     * 访问控制 e 租户查询
     *
     * @param mallCategory 产品分类
     * @return 产品分类集合
     */
    @DataScope(SWeAlias = "e")
    public List<MallCategory> selectMallCategoryTree(MallCategory mallCategory);

    /**
     * 根据Id查询产品分类子集
     * 访问控制 e 租户查询
     *
     * @param mallCategory 产品分类
     * @return 产品分类集合
     */
    @DataScope(SWeAlias = "e")
    public List<MallCategory> selectChildrenById(MallCategory mallCategory);

    /**
     * 查询产品分类
     * 访问控制 e 租户查询
     *
     * @param mallCategory 产品分类
     * @return 产品分类
     */
    @DataScope(SWeAlias = "e")
    public MallCategory selectMallCategoryById(MallCategory mallCategory);

    /**
     * 新增产品分类
     * 访问控制 empty 租户更新（无前缀）
     *
     * @param mallCategory 产品分类
     * @return 结果
     */
    @DataScope(SWueAlias = "empty")
    public int insertMallCategory(MallCategory mallCategory);

    /**
     * 修改产品分类
     * 访问控制 empty 租户更新（无前缀）
     *
     * @param mallCategory 产品分类
     * @return 结果
     */
    @DataScope(SWueAlias = "empty")
    public int updateMallCategory(MallCategory mallCategory);

    /**
     * 修改产品分类子集祖籍信息
     * 访问控制 empty 租户更新（无前缀）
     *
     * @param mallCategory 产品分类 | List<MallCategory> children
     * @return 结果
     */
    @DataScope(SWueAlias = "empty")
    public int updateChildrenAncestors(MallCategory mallCategory);

    /**
     * 删除产品分类
     * 访问控制 empty 租户更新（无前缀）
     *
     * @param mallCategory 产品分类
     * @return 结果
     */
    @DataScope(SWueAlias = "empty")
    public int deleteMallCategoryById(MallCategory mallCategory);

    /**
     * 批量删除产品分类
     * 访问控制 empty 租户更新（无前缀）
     *
     * @param mallCategory 产品分类
     * @return 结果
     */
    @DataScope(SWueAlias = "empty")
    public int deleteMallCategoryByIds(MallCategory mallCategory);

    /**
     * 根据Id删除产品分类子集
     * 访问控制 empty 租户更新（无前缀）
     *
     * @param mallCategory 产品分类
     * @return 结果
     */
    @DataScope(SWueAlias = "empty")
    public int deleteChildrenById(MallCategory mallCategory);
}