package com.xueyi.product.mapper;

import com.xueyi.common.datascope.annotation.DataScope;
import com.xueyi.product.api.MallLibrary;

import java.util.List;

/**
 * 产品库 数据层
 *
 * @author xueyi
 */
public interface MallLibraryMapper {

    /**
     * 查询全部产品库
     * 访问控制 l 租户|系统|站点查询
     *
     * @param mallLibrary 产品库
     * @return 产品库集合
     */
    @DataScope(SWeAlias = "l")
    public List<MallLibrary> selectAllList(MallLibrary mallLibrary);

    /**
     * 查询产品库
     * 访问控制 l 租户|系统|站点查询
     *
     * @param mallLibrary 产品库
     * @return 产品库
     */
    @DataScope(SWeAlias = "l")
    public MallLibrary selectMallLibraryById(MallLibrary mallLibrary);

    /**
     * 查询产品库列表
     * 访问控制 l 租户|系统|站点查询
     *
     * @param mallLibrary 产品库
     * @return 产品库集合
     */
    @DataScope(SWeAlias = "l")
    public List<MallLibrary> selectMallLibraryList(MallLibrary mallLibrary);

    /**
     * 新增产品库
     * 访问控制 s 租户|系统|站点查询
     *
     * @param mallLibrary 产品库
     * @return 结果
     */
    @DataScope(SWueAlias = "empty")
    public int insertMallLibrary(MallLibrary mallLibrary);

    /**
     * 修改产品库
     * 访问控制 empty 租户|系统|站点更新（无前缀）
     *
     * @param mallLibrary 产品库
     * @return 结果
     */
    @DataScope(SWueAlias = "empty")
    public int updateMallLibrary(MallLibrary mallLibrary);

    /**
     * 修改产品库排序
     * 访问控制 empty 租户|系统|站点更新（无前缀）
     *
     * @param mallLibrary 产品库
     * @return 结果
     */
    @DataScope(SWueAlias = "empty")
    public int updateMallLibrarySort(MallLibrary mallLibrary);

    /**
     * 删除产品库
     * 访问控制 empty 租户|系统|站点更新（无前缀）
     *
     * @param mallLibrary 产品库
     * @return 结果
     */
    @DataScope(SWueAlias = "empty")
    public int deleteMallLibraryById(MallLibrary mallLibrary);

    /**
     * 批量删除产品库
     * 访问控制 empty 租户|系统|站点更新（无前缀）
     *
     * @param mallLibrary 产品库
     * @return 结果
     */
    @DataScope(SWueAlias = "empty")
    public int deleteMallLibraryByIds(MallLibrary mallLibrary);
}