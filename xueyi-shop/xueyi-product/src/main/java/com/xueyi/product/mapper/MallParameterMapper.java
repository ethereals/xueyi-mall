package com.xueyi.product.mapper;

import java.util.List;

import com.xueyi.common.datascope.annotation.DataScope;
import com.xueyi.product.domain.MallParameter;

/**
 * 产品参数Mapper接口
 *
 * @author xueyi
 */
public interface MallParameterMapper {

    /**
     * 查询产品参数列表赋予产品
     * 访问控制 p 租户|系统|站点|产品库查询
     *
     * @param mallParameter 产品参数
     * @return 产品参数集合
     */
    @DataScope(SWLeAlias = "p")
    public List<MallParameter> selectMallParameterListToProduct(MallParameter mallParameter);

    /**
     * 查询产品参数
     * 访问控制 p 租户|系统|站点|产品库查询
     *
     * @param mallParameter 产品参数
     * @return 产品参数
     */
    @DataScope(SWLeAlias = "p")
    public MallParameter selectMallParameterById(MallParameter mallParameter);

    /**
     * 查询产品参数列表
     * 访问控制 p 租户|系统|站点|产品库查询
     *
     * @param mallParameter 产品参数
     * @return 产品参数集合
     */
    @DataScope(SWLeAlias = "p")
    public List<MallParameter> selectMallParameterList(MallParameter mallParameter);

    /**
     * 新增产品参数
     * 访问控制 empty 租户更新（无前缀）()控制方法在impl层 | MallParameterServiceImpl)
     *
     * @param mallParameter 产品参数
     * @return 结果
     */
    public int insertMallParameter(MallParameter mallParameter);

    /**
     * 修改产品参数
     * 访问控制 empty 租户|系统|站点|产品库更新（无前缀）
     *
     * @param mallParameter 产品参数
     * @return 结果
     */
    @DataScope(SWLueAlias = "empty")
    public int updateMallParameter(MallParameter mallParameter);

    /**
     * 修改产品参数排序
     * 访问控制 empty 租户|系统|站点|产品库更新（无前缀）
     *
     * @param mallParameter 产品参数
     * @return 结果
     */
    @DataScope(SWLueAlias = "empty")
    public int updateMallParameterSort(MallParameter mallParameter);

    /**
     * 删除产品参数
     * 访问控制 empty 租户|系统|站点|产品库更新（无前缀）
     *
     * @param mallParameter 产品参数
     * @return 结果
     */
    @DataScope(SWLueAlias = "empty")
    public int deleteMallParameterById(MallParameter mallParameter);

    /**
     * 批量删除产品参数
     * 访问控制 empty 租户|系统|站点|产品库更新（无前缀）
     *
     * @param mallParameter 产品参数
     * @return 结果
     */
    @DataScope(SWLueAlias = "empty")
    public int deleteMallParameterByIds(MallParameter mallParameter);

    /**
     * 批量新增产品参数值
     * 访问控制 empty 租户更新（无前缀）
     *
     * @param mallParameter 产品参数
     * @return 结果
     */
    @DataScope(SWLueAlias = "empty")
    public int batchMallParameterValue(MallParameter mallParameter);

    /**
     * 通过产品参数Id删除产品参数值信息
     * 访问控制 empty 租户更新（无前缀）
     *
     * @param mallParameter 产品参数
     * @return 结果
     */
    @DataScope(SWLueAlias = "empty")
    public int deleteMallParameterValueByParameterId(MallParameter mallParameter);

    /**
     * 批量删除产品参数值
     * 访问控制 empty 租户更新（无前缀）
     *
     * @param mallParameter 产品参数
     * @return 结果
     */
    @DataScope(SWLueAlias = "empty")
    public int deleteMallParameterValueByParameterIds(MallParameter mallParameter);
}