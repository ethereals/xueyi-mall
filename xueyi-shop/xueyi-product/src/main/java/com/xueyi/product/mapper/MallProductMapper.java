package com.xueyi.product.mapper;

import java.util.List;

import com.xueyi.common.datascope.annotation.DataScope;
import com.xueyi.product.domain.MallProduct;

/**
 * 产品 数据层
 *
 * @author xueyi
 */
public interface MallProductMapper {
    /**
     * 查询产品列表
     * 访问控制 e 租户查询
     *
     * @param mallProduct 产品
     * @return 产品集合
     */
    @DataScope(SWLeAlias = "e")
    public List<MallProduct> selectMallProductList(MallProduct mallProduct);

    /**
     * 查询产品
     * 访问控制 e 租户查询
     *
     * @param mallProduct 产品
     * @return 产品
     */
    @DataScope(SWLeAlias = "e")
    public MallProduct selectMallProductById(MallProduct mallProduct);

    /**
     * 新增产品
     * 访问控制 empty 租户更新（无前缀）
     *
     * @param mallProduct 产品
     * @return 结果
     */
    @DataScope(SWLueAlias = "empty")
    public int insertMallProduct(MallProduct mallProduct);

    /**
     * 修改产品
     * 访问控制 empty 租户更新（无前缀）
     *
     * @param mallProduct 产品
     * @return 结果
     */
    @DataScope(SWLueAlias = "empty")
    public int updateMallProduct(MallProduct mallProduct);

    /**
     * 修改产品排序
     * 访问控制 empty 租户更新（无前缀）
     *
     * @param mallProduct 产品
     * @return 结果
     */
    @DataScope(SWLueAlias = "empty")
    public int updateMallProductSort(MallProduct mallProduct);

    /**
     * 删除产品
     * 访问控制 empty 租户更新（无前缀）
     *
     * @param mallProduct 产品
     * @return 结果
     */
    @DataScope(SWLueAlias = "empty")
    public int deleteMallProductById(MallProduct mallProduct);

    /**
     * 批量删除产品
     * 访问控制 empty 租户更新（无前缀）
     *
     * @param mallProduct 产品
     * @return 结果
     */
    @DataScope(SWLueAlias = "empty")
    public int deleteMallProductByIds(MallProduct mallProduct);
}