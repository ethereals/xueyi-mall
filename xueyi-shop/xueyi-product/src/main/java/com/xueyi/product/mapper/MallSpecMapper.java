package com.xueyi.product.mapper;

import java.util.List;

import com.xueyi.common.datascope.annotation.DataScope;
import com.xueyi.product.domain.MallSpec;

/**
 * 产品规格Mapper接口
 *
 * @author xueyi
 */
public interface MallSpecMapper {

    /**
     * 查询产品规格列表赋予产品
     * 访问控制 s 租户|系统|站点|产品库查询
     *
     * @param mallSpec 产品规格
     * @return 产品规格
     */
    @DataScope(SWLeAlias = "s")
    public List<MallSpec> selectMallSpecListToProduct(MallSpec mallSpec);

    /**
     * 查询产品规格
     * 访问控制 s 租户|系统|站点|产品库查询
     *
     * @param mallSpec 产品规格
     * @return 产品规格
     */
    @DataScope(SWLeAlias = "s")
    public MallSpec selectMallSpecById(MallSpec mallSpec);

    /**
     * 查询产品规格列表
     * 访问控制 s 租户|系统|站点|产品库查询
     *
     * @param mallSpec 产品规格
     * @return 产品规格集合
     */
    @DataScope(SWLeAlias = "s")
    public List<MallSpec> selectMallSpecList(MallSpec mallSpec);

    /**
     * 新增产品规格
     * 访问控制 empty 租户更新（无前缀）()控制方法在impl层 | MallSpecServiceImpl)
     *
     * @param mallSpec 产品规格
     * @return 结果
     */
    public int insertMallSpec(MallSpec mallSpec);

    /**
     * 修改产品规格
     * 访问控制 empty 租户|系统|站点|产品库更新（无前缀）
     *
     * @param mallSpec 产品规格
     * @return 结果
     */
    @DataScope(SWLueAlias = "empty")
    public int updateMallSpec(MallSpec mallSpec);

    /**
     * 修改产品规格排序
     * 访问控制 empty 租户|系统|站点|产品库更新（无前缀）
     *
     * @param mallSpec 产品规格
     * @return 结果
     */
    @DataScope(SWLueAlias = "empty")
    public int updateMallSpecSort(MallSpec mallSpec);

    /**
     * 删除产品规格
     * 访问控制 empty 租户|系统|站点|产品库更新（无前缀）
     *
     * @param mallSpec 产品规格
     * @return 结果
     */
    @DataScope(SWLueAlias = "empty")
    public int deleteMallSpecById(MallSpec mallSpec);

    /**
     * 批量删除产品规格
     * 访问控制 empty 租户|系统|站点|产品库更新（无前缀）
     *
     * @param mallSpec 产品规格
     * @return 结果
     */
    @DataScope(SWLueAlias = "empty")
    public int deleteMallSpecByIds(MallSpec mallSpec);

    /**
     * 批量新增产品规格值
     * 访问控制 empty 租户更新（无前缀）
     *
     * @param mallSpec 产品规格
     * @return 结果
     */
    @DataScope(SWLueAlias = "empty")
    public int batchMallSpecValue(MallSpec mallSpec);

    /**
     * 通过产品规格Id删除产品规格值信息
     * 访问控制 empty 租户更新（无前缀）
     *
     * @param mallSpec 产品规格
     * @return 结果
     */
    @DataScope(SWLueAlias = "empty")
    public int deleteMallSpecValueBySpecId(MallSpec mallSpec);

    /**
     * 批量删除产品规格值
     * 访问控制 empty 租户更新（无前缀）
     *
     * @param mallSpec 产品规格
     * @return 结果
     */
    @DataScope(SWLueAlias = "empty")
    public int deleteMallSpecValueBySpecIds(MallSpec mallSpec);
}