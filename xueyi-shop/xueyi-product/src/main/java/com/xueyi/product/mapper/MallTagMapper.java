package com.xueyi.product.mapper;

import java.util.List;

import com.xueyi.common.datascope.annotation.DataScope;
import com.xueyi.product.domain.MallTag;

/**
 * 产品标签 数据层
 *
 * @author xueyi
 */
public interface MallTagMapper {
    /**
     * 查询产品标签列表
     * 访问控制 e 租户查询
     *
     * @param mallTag 产品标签
     * @return 产品标签集合
     */
    @DataScope(SWeAlias = "e")
    public List<MallTag> selectMallTagList(MallTag mallTag);

    /**
     * 查询产品标签
     * 访问控制 e 租户查询
     *
     * @param mallTag 产品标签
     * @return 产品标签
     */
    @DataScope(SWeAlias = "e")
    public MallTag selectMallTagById(MallTag mallTag);

    /**
     * 新增产品标签
     * 访问控制 empty 租户更新（无前缀）
     *
     * @param mallTag 产品标签
     * @return 结果
     */
    @DataScope(SWueAlias = "empty")
    public int insertMallTag(MallTag mallTag);

    /**
     * 修改产品标签
     * 访问控制 empty 租户更新（无前缀）
     *
     * @param mallTag 产品标签
     * @return 结果
     */
    @DataScope(SWueAlias = "empty")
    public int updateMallTag(MallTag mallTag);

    /**
     * 修改产品标签排序
     * 访问控制 empty 租户更新（无前缀）
     *
     * @param mallTag 产品标签
     * @return 结果
     */
    @DataScope(SWueAlias = "empty")
    public int updateMallTagSort(MallTag mallTag);

    /**
     * 删除产品标签
     * 访问控制 empty 租户更新（无前缀）
     *
     * @param mallTag 产品标签
     * @return 结果
     */
    @DataScope(SWueAlias = "empty")
    public int deleteMallTagById(MallTag mallTag);

    /**
     * 批量删除产品标签
     * 访问控制 empty 租户更新（无前缀）
     *
     * @param mallTag 产品标签
     * @return 结果
     */
    @DataScope(SWueAlias = "empty")
    public int deleteMallTagByIds(MallTag mallTag);
}