package com.xueyi.product.service;

import java.util.List;
import com.xueyi.product.domain.MallCategory;

/**
 * 产品分类 业务层
 *
 * @author xueyi
 */
public interface IMallCategoryService
{
    /**
     * 查询产品分类列表
     *
     * @param mallCategory 产品分类
     * @return 产品分类集合
     */
    public List<MallCategory> selectMallCategoryList(MallCategory mallCategory);

    /**
     * 查询产品分类树选项列表
     *
     * @param mallCategory 产品分类
     * @return 产品分类集合
     */
    public List<MallCategory> selectMallCategoryTree(MallCategory mallCategory);

    /**
     * 查询产品分类
     *
     * @param mallCategory 产品分类
     * @return 产品分类
     */
    public MallCategory selectMallCategoryById(MallCategory mallCategory);

    /**
     * 新增产品分类
     *
     * @param mallCategory 产品分类
     * @return 结果
     */
    public int insertMallCategory(MallCategory mallCategory);

    /**
     * 修改产品分类
     *
     * @param mallCategory 产品分类
     * @return 结果
     */
    public int updateMallCategory(MallCategory mallCategory);

    /**
     * 删除产品分类信息
     *
     * @param mallCategory 产品分类
     * @return 结果
     */
    public int deleteMallCategoryById(MallCategory mallCategory);
}