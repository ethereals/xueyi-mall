package com.xueyi.product.service;

import java.util.List;

import com.xueyi.product.api.MallLibrary;

/**
 * 产品库 业务层
 *
 * @author xueyi
 */
public interface IMallLibraryService {
    /**
     * 查询全部产品库
     *
     * @param mallLibrary 产品库
     * @return 产品库集合
     */
    public List<MallLibrary> selectAllList(MallLibrary mallLibrary);

    /**
     * 查询产品库
     *
     * @param mallLibrary 产品库
     * @return 产品库
     */
    public MallLibrary selectMallLibraryById(MallLibrary mallLibrary);

    /**
     * 查询产品库列表
     *
     * @param mallLibrary 产品库
     * @return 产品库集合
     */
    public List<MallLibrary> selectMallLibraryList(MallLibrary mallLibrary);

    /**
     * 新增产品库
     *
     * @param mallLibrary 产品库
     * @return 结果
     */
    public int insertMallLibrary(MallLibrary mallLibrary);

    /**
     * 修改产品库
     *
     * @param mallLibrary 产品库
     * @return 结果
     */
    public int updateMallLibrary(MallLibrary mallLibrary);

    /**
     * 修改产品库排序
     *
     * @param mallLibrary 产品库
     * @return 结果
     */
    public int updateMallLibrarySort(MallLibrary mallLibrary);

    /**
     * 批量删除产品库
     *
     * @param mallLibrary 产品库
     * @return 结果
     */
    public int deleteMallLibraryByIds(MallLibrary mallLibrary);

    /**
     * 删除产品库信息
     *
     * @param mallLibrary 产品库
     * @return 结果
     */
    public int deleteMallLibraryById(MallLibrary mallLibrary);
}
