package com.xueyi.product.service;

import java.util.List;

import com.xueyi.common.datascope.annotation.DataScope;
import com.xueyi.product.domain.MallParameter;

/**
 * 产品参数Service接口
 *
 * @author xueyi
 */
public interface IMallParameterService {

    /**
     * 查询产品参数列表赋予产品
     *
     * @param mallParameter 产品参数
     * @return 产品参数集合
     */
    public List<MallParameter> selectMallParameterListToProduct(MallParameter mallParameter);

    /**
     * 查询产品参数
     *
     * @param mallParameter 产品参数
     * @return 产品参数
     */
    public MallParameter selectMallParameterById(MallParameter mallParameter);

    /**
     * 查询产品参数列表
     *
     * @param mallParameter 产品参数
     * @return 产品参数集合
     */
    public List<MallParameter> selectMallParameterList(MallParameter mallParameter);

    /**
     * 新增产品参数
     *
     * @param mallParameter 产品参数
     * @return 结果
     */
    public int insertMallParameter(MallParameter mallParameter);

    /**
     * 修改产品参数
     *
     * @param mallParameter 产品参数
     * @return 结果
     */
    public int updateMallParameter(MallParameter mallParameter);


    /**
     * 修改产品参数排序
     *
     * @param mallParameter 产品参数
     * @return 结果
     */
    public int updateMallParameterSort(MallParameter mallParameter);

    /**
     * 批量删除产品参数
     *
     * @param mallParameter 产品参数
     * @return 结果
     */
    public int deleteMallParameterByIds(MallParameter mallParameter);

    /**
     * 删除产品参数信息
     *
     * @param mallParameter 产品参数
     * @return 结果
     */
    public int deleteMallParameterById(MallParameter mallParameter);
}