package com.xueyi.product.service;

import java.util.List;

import com.xueyi.product.domain.MallProduct;

/**
 * 产品 业务层
 *
 * @author xueyi
 */
public interface IMallProductService {
    /**
     * 查询产品列表
     *
     * @param mallProduct 产品
     * @return 产品集合
     */
    public List<MallProduct> selectMallProductList(MallProduct mallProduct);


    /**
     * 查询产品
     *
     * @param mallProduct 产品
     * @return 产品
     */
    public MallProduct selectMallProductById(MallProduct mallProduct);

    /**
     * 新增产品
     *
     * @param mallProduct 产品
     * @return 结果
     */
    public int insertMallProduct(MallProduct mallProduct);

    /**
     * 修改产品
     *
     * @param mallProduct 产品
     * @return 结果
     */
    public int updateMallProduct(MallProduct mallProduct);

    /**
     * 修改产品排序
     *
     * @param mallProduct 产品
     * @return 结果
     */
    public int updateMallProductSort(MallProduct mallProduct);

    /**
     * 删除产品信息
     *
     * @param mallProduct 产品
     * @return 结果
     */
    public int deleteMallProductById(MallProduct mallProduct);

    /**
     * 批量删除产品
     *
     * @param mallProduct 产品
     * @return 结果
     */
    public int deleteMallProductByIds(MallProduct mallProduct);
}