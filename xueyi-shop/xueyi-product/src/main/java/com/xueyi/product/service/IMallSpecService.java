package com.xueyi.product.service;

import java.util.List;

import com.xueyi.product.domain.MallSpec;

/**
 * 产品规格Service接口
 *
 * @author xueyi
 */
public interface IMallSpecService {

    /**
     * 查询产品规格列表赋予产品
     *
     * @param mallSpec 产品规格
     * @return 产品规格集合
     */
    public List<MallSpec> selectMallSpecListToProduct(MallSpec mallSpec);

    /**
     * 查询产品规格
     *
     * @param mallSpec 产品规格
     * @return 产品规格
     */
    public MallSpec selectMallSpecById(MallSpec mallSpec);

    /**
     * 查询产品规格列表
     *
     * @param mallSpec 产品规格
     * @return 产品规格集合
     */
    public List<MallSpec> selectMallSpecList(MallSpec mallSpec);

    /**
     * 新增产品规格
     *
     * @param mallSpec 产品规格
     * @return 结果
     */
    public int insertMallSpec(MallSpec mallSpec);

    /**
     * 修改产品规格
     *
     * @param mallSpec 产品规格
     * @return 结果
     */
    public int updateMallSpec(MallSpec mallSpec);

    /**
     * 修改产品规格排序
     *
     * @param mallSpec 产品规格
     * @return 结果
     */
    public int updateMallSpecSort(MallSpec mallSpec);

    /**
     * 批量删除产品规格
     *
     * @param mallSpec 产品规格
     * @return 结果
     */
    public int deleteMallSpecByIds(MallSpec mallSpec);

    /**
     * 删除产品规格信息
     *
     * @param mallSpec 产品规格
     * @return 结果
     */
    public int deleteMallSpecById(MallSpec mallSpec);
}