package com.xueyi.product.service;

import java.util.List;
import com.xueyi.product.domain.MallTag;

/**
 * 产品标签 业务层
 *
 * @author xueyi
 */
public interface IMallTagService
{
    /**
     * 查询产品标签列表
     *
     * @param mallTag 产品标签
     * @return 产品标签集合
     */
    public List<MallTag> selectMallTagList(MallTag mallTag);


    /**
     * 查询产品标签
     *
     * @param mallTag 产品标签
     * @return 产品标签
     */
    public MallTag selectMallTagById(MallTag mallTag);

    /**
     * 新增产品标签
     *
     * @param mallTag 产品标签
     * @return 结果
     */
    public int insertMallTag(MallTag mallTag);

    /**
     * 修改产品标签
     *
     * @param mallTag 产品标签
     * @return 结果
     */
    public int updateMallTag(MallTag mallTag);

    /**
     * 修改产品标签排序
     *
     * @param mallTag 产品标签
     * @return 结果
     */
    public int updateMallTagSort(MallTag mallTag);

    /**
     * 删除产品标签信息
     *
     * @param mallTag 产品标签
     * @return 结果
     */
    public int deleteMallTagById(MallTag mallTag);

    /**
     * 批量删除产品标签
     *
     * @param mallTag 产品标签
     * @return 结果
     */
    public int deleteMallTagByIds(MallTag mallTag);
}