package com.xueyi.product.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.xueyi.product.mapper.MallCategoryMapper;
import com.xueyi.product.domain.MallCategory;
import com.xueyi.product.service.IMallCategoryService;

/**
 * 产品分类 业务层处理
 *
 * @author xueyi
 */
@Service
public class MallCategoryServiceImpl implements IMallCategoryService {
    @Autowired
    private MallCategoryMapper mallCategoryMapper;

    /**
     * 查询产品分类列表
     *
     * @param mallCategory 产品分类
     * @return 产品分类
     */
    @Override
    public List<MallCategory> selectMallCategoryList(MallCategory mallCategory) {
        return mallCategoryMapper.selectMallCategoryList(mallCategory);
    }

    /**
     * 查询产品分类树选项列表
     *
     * @param mallCategory 产品分类
     * @return 产品分类
     */
    @Override
    public List<MallCategory> selectMallCategoryTree(MallCategory mallCategory) {
        return mallCategoryMapper.selectMallCategoryTree(mallCategory);
    }

    /**
     * 查询产品分类
     *
     * @param mallCategory 产品分类
     * @return 产品分类
     */
    @Override
    public MallCategory selectMallCategoryById(MallCategory mallCategory) {
        return mallCategoryMapper.selectMallCategoryById(mallCategory);
    }

    /**
     * 新增产品分类
     *
     * @param mallCategory 产品分类
     * @return 结果
     */
    @Override
    public int insertMallCategory(MallCategory mallCategory) {
        if (!mallCategory.getParentId().equals(0L)) {
            MallCategory search = new MallCategory();
            search.setCategoryId(mallCategory.getParentId());
            MallCategory newParent = mallCategoryMapper.selectMallCategoryById(search);
            mallCategory.setAncestors(newParent.getAncestors() + "," + newParent.getCategoryId());
        } else {
            mallCategory.setAncestors("0");
        }
        return mallCategoryMapper.insertMallCategory(mallCategory);
    }

    /**
     * 修改产品分类
     *
     * @param mallCategory 产品分类
     * @return 结果
     */
    @Override
    public int updateMallCategory(MallCategory mallCategory) {
        MallCategory search = new MallCategory();
        search.setCategoryId(mallCategory.getCategoryId());
        MallCategory oldMallCategory = mallCategoryMapper.selectMallCategoryById(search);
        if (!oldMallCategory.getParentId().equals(mallCategory.getParentId())) {
            String newAncestors, oldAncestors;
            if (oldMallCategory.getParentId().equals(0L)) {
                oldAncestors = "0";
            } else {
                search.setCategoryId(oldMallCategory.getParentId());
                MallCategory oldParent = mallCategoryMapper.selectMallCategoryById(search);
                oldAncestors = oldParent.getAncestors() + "," + oldParent.getCategoryId();
            }
            if (mallCategory.getParentId().equals(0L)) {
                newAncestors = "0";
            } else {
                search.setCategoryId(mallCategory.getParentId());
                MallCategory newParent = mallCategoryMapper.selectMallCategoryById(search);
                newAncestors = newParent.getAncestors() + "," + newParent.getCategoryId();
            }
            mallCategory.setAncestors(newAncestors);
            updateMallCategoryChildren(mallCategory.getCategoryId(), newAncestors, oldAncestors);
        }
        return mallCategoryMapper.updateMallCategory(mallCategory);
    }

    /**
     * 删除产品分类信息
     *
     * @param mallCategory 产品分类
     * @return 结果
     */
    @Override
    public int deleteMallCategoryById(MallCategory mallCategory) {
        mallCategoryMapper.deleteChildrenById(mallCategory);
        return mallCategoryMapper.deleteMallCategoryById(mallCategory);
    }

    /**
     * 修改子元素关系
     *
     * @param categoryId   被修改的产品分类Id
     * @param newAncestors 新的父Id集合
     * @param oldAncestors 旧的父Id集合
     */
    public void updateMallCategoryChildren(Long categoryId, String newAncestors, String oldAncestors) {
        MallCategory searchChild = new MallCategory();
        searchChild.setCategoryId(categoryId);
        List<MallCategory> children = mallCategoryMapper.selectChildrenById(searchChild);
        if (children.size() > 0) {
            for (MallCategory child : children) {
                child.setAncestors(child.getAncestors().replaceFirst(oldAncestors, newAncestors));
            }
            searchChild.setCategoryId(null);
            searchChild.getParams().put("children", children);
            mallCategoryMapper.updateChildrenAncestors(searchChild);
        }
    }
}