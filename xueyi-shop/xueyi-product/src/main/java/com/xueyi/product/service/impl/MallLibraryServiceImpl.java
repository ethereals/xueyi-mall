package com.xueyi.product.service.impl;

import java.util.List;

import com.xueyi.product.mapper.MallLibraryMapper;
import com.xueyi.product.service.IMallLibraryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.xueyi.product.api.MallLibrary;

/**
 * 产品库 业务层处理
 *
 * @author xueyi
 */
@Service
public class MallLibraryServiceImpl implements IMallLibraryService {
    @Autowired
    private MallLibraryMapper mallLibraryMapper;

    /**
     * 查询全部产品库
     *
     * @param mallLibrary 产品库
     * @return 产品库集合
     */
    public List<MallLibrary> selectAllList(MallLibrary mallLibrary) {
        return mallLibraryMapper.selectAllList(mallLibrary);
    }

    /**
     * 查询产品库
     *
     * @param mallLibrary 产品库
     * @return 产品库
     */
    @Override
    public MallLibrary selectMallLibraryById(MallLibrary mallLibrary) {
        return mallLibraryMapper.selectMallLibraryById(mallLibrary);
    }

    /**
     * 查询产品库列表
     *
     * @param mallLibrary 产品库
     * @return 产品库
     */
    @Override
    public List<MallLibrary> selectMallLibraryList(MallLibrary mallLibrary) {
        return mallLibraryMapper.selectMallLibraryList(mallLibrary);
    }

    /**
     * 新增产品库
     *
     * @param mallLibrary 产品库
     * @return 结果
     */
    @Override
    public int insertMallLibrary(MallLibrary mallLibrary) {
        return mallLibraryMapper.insertMallLibrary(mallLibrary);
    }

    /**
     * 修改产品库
     *
     * @param mallLibrary 产品库
     * @return 结果
     */
    @Override
    public int updateMallLibrary(MallLibrary mallLibrary) {
        return mallLibraryMapper.updateMallLibrary(mallLibrary);
    }

    /**
     * 修改产品库排序
     *
     * @param mallLibrary 产品库
     * @return 结果
     */
    @Override
    public int updateMallLibrarySort(MallLibrary mallLibrary) {
        return mallLibraryMapper.updateMallLibrarySort(mallLibrary);
    }

    /**
     * 批量删除产品库
     *
     * @param mallLibrary 产品库
     * @return 结果
     */
    @Override
    public int deleteMallLibraryByIds(MallLibrary mallLibrary) {
        return mallLibraryMapper.deleteMallLibraryByIds(mallLibrary);
    }

    /**
     * 删除产品库信息
     *
     * @param mallLibrary 产品库
     * @return 结果
     */
    @Override
    public int deleteMallLibraryById(MallLibrary mallLibrary) {
        return mallLibraryMapper.deleteMallLibraryById(mallLibrary);
    }
}
