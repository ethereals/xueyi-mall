package com.xueyi.product.service.impl;

import java.util.List;

import com.xueyi.common.datascope.annotation.DataScope;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.xueyi.product.mapper.MallParameterMapper;
import com.xueyi.product.domain.MallParameter;
import com.xueyi.product.service.IMallParameterService;
import org.springframework.transaction.annotation.Transactional;

/**
 * 产品参数Service业务层处理
 *
 * @author xueyi
 */
@Service
public class MallParameterServiceImpl implements IMallParameterService {

    @Autowired
    private MallParameterMapper mallParameterMapper;

    /**
     * 查询产品参数列表赋予产品
     *
     * @param mallParameter 产品参数
     * @return 产品参数集合
     */
    @Override
    public List<MallParameter> selectMallParameterListToProduct(MallParameter mallParameter) {
        return mallParameterMapper.selectMallParameterListToProduct(mallParameter);
    }

    /**
     * 查询产品参数
     *
     * @param mallParameter 产品参数
     * @return 产品参数
     */
    @Override
    public MallParameter selectMallParameterById(MallParameter mallParameter) {
        return mallParameterMapper.selectMallParameterById(mallParameter);
    }

    /**
     * 查询产品参数列表
     *
     * @param mallParameter 产品参数
     * @return 产品参数
     */
    @Override
    public List<MallParameter> selectMallParameterList(MallParameter mallParameter) {
        return mallParameterMapper.selectMallParameterList(mallParameter);
    }

    /**
     * 新增产品参数
     *
     * @param mallParameter 产品参数
     * @return 结果
     */
    @Override
    @Transactional
    @DataScope(SWLueAlias = "empty")
    public int insertMallParameter(MallParameter mallParameter) {
        int rows = mallParameterMapper.insertMallParameter(mallParameter);
        if (mallParameter.getValues().size() > 0 && mallParameter.getFieldType().equals("0")) {
            /**获取生成雪花Id，并赋值给主键，加入至子表对应外键中*/
            mallParameter.setParameterId(mallParameter.getId());
            mallParameterMapper.batchMallParameterValue(mallParameter);
        }
        return rows;
    }

    /**
     * 修改产品参数
     *
     * @param mallParameter 产品参数
     * @return 结果
     */
    @Override
    @Transactional
    public int updateMallParameter(MallParameter mallParameter) {
        mallParameterMapper.deleteMallParameterValueByParameterId(mallParameter);
        if (mallParameter.getValues().size() > 0 && mallParameter.getFieldType().equals("0")) {
            mallParameterMapper.batchMallParameterValue(mallParameter);
        }
        return mallParameterMapper.updateMallParameter(mallParameter);
    }

    /**
     * 修改产品参数排序
     *
     * @param mallParameter 产品参数
     * @return 结果
     */
    @Override
    @Transactional
    public int updateMallParameterSort(MallParameter mallParameter) {
        return mallParameterMapper.updateMallParameterSort(mallParameter);
    }

    /**
     * 删除产品参数信息
     *
     * @param mallParameter 产品参数
     * @return 结果
     */
    @Override
    @Transactional
    public int deleteMallParameterById(MallParameter mallParameter) {
        mallParameterMapper.deleteMallParameterValueByParameterId(mallParameter);
        return mallParameterMapper.deleteMallParameterById(mallParameter);
    }

    /**
     * 批量删除产品参数
     *
     * @param mallParameter 产品参数
     * @return 结果
     */
    @Override
    @Transactional
    public int deleteMallParameterByIds(MallParameter mallParameter) {
        mallParameterMapper.deleteMallParameterValueByParameterIds(mallParameter);
        return mallParameterMapper.deleteMallParameterByIds(mallParameter);
    }
}