package com.xueyi.product.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.xueyi.product.mapper.MallProductMapper;
import com.xueyi.product.domain.MallProduct;
import com.xueyi.product.service.IMallProductService;

/**
 * 产品 业务层处理
 *
 * @author xueyi
 */
@Service
public class MallProductServiceImpl implements IMallProductService {
    @Autowired
    private MallProductMapper mallProductMapper;

    /**
     * 查询产品列表
     *
     * @param mallProduct 产品
     * @return 产品
     */
    @Override
    public List<MallProduct> selectMallProductList(MallProduct mallProduct) {
        return mallProductMapper.selectMallProductList(mallProduct);
    }


    /**
     * 查询产品
     *
     * @param mallProduct 产品
     * @return 产品
     */
    @Override
    public MallProduct selectMallProductById(MallProduct mallProduct) {
        return mallProductMapper.selectMallProductById(mallProduct);
    }

    /**
     * 新增产品
     *
     * @param mallProduct 产品
     * @return 结果
     */
    @Override
    public int insertMallProduct(MallProduct mallProduct) {
        return mallProductMapper.insertMallProduct(mallProduct);
    }

    /**
     * 修改产品
     *
     * @param mallProduct 产品
     * @return 结果
     */
    @Override
    public int updateMallProduct(MallProduct mallProduct) {
        return mallProductMapper.updateMallProduct(mallProduct);
    }

    /**
     * 修改产品排序
     *
     * @param mallProduct 产品
     * @return 结果
     */
    @Override
    public int updateMallProductSort(MallProduct mallProduct) {
        return mallProductMapper.updateMallProductSort(mallProduct);
    }

    /**
     * 删除产品信息
     *
     * @param mallProduct 产品
     * @return 结果
     */
    @Override
    public int deleteMallProductById(MallProduct mallProduct) {
        return mallProductMapper.deleteMallProductById(mallProduct);
    }

    /**
     * 批量删除产品
     *
     * @param mallProduct 产品
     * @return 结果
     */
    @Override
    public int deleteMallProductByIds(MallProduct mallProduct) {
        return mallProductMapper.deleteMallProductByIds(mallProduct);
    }
}