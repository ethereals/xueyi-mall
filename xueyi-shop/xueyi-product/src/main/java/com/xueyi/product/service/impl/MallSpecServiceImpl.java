package com.xueyi.product.service.impl;

import java.util.List;

import com.xueyi.common.datascope.annotation.DataScope;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.xueyi.product.mapper.MallSpecMapper;
import com.xueyi.product.domain.MallSpec;
import com.xueyi.product.service.IMallSpecService;
import org.springframework.transaction.annotation.Transactional;

/**
 * 产品规格Service业务层处理
 *
 * @author xueyi
 */
@Service
public class MallSpecServiceImpl implements IMallSpecService {

    @Autowired
    private MallSpecMapper mallSpecMapper;

    /**
     * 查询产品规格列表赋予产品
     *
     * @param mallSpec 产品规格
     * @return 产品规格集合
     */
    public List<MallSpec> selectMallSpecListToProduct(MallSpec mallSpec) {
        return mallSpecMapper.selectMallSpecListToProduct(mallSpec);
    }

    /**
     * 查询产品规格
     *
     * @param mallSpec 产品规格
     * @return 产品规格
     */
    @Override
    public MallSpec selectMallSpecById(MallSpec mallSpec) {
        return mallSpecMapper.selectMallSpecById(mallSpec);
    }

    /**
     * 查询产品规格列表
     *
     * @param mallSpec 产品规格
     * @return 产品规格
     */
    @Override
    public List<MallSpec> selectMallSpecList(MallSpec mallSpec) {
        return mallSpecMapper.selectMallSpecList(mallSpec);
    }

    /**
     * 新增产品规格
     *
     * @param mallSpec 产品规格
     * @return 结果
     */
    @Override
    @Transactional
    @DataScope(SWLueAlias = "empty")
    public int insertMallSpec(MallSpec mallSpec) {
        int rows = mallSpecMapper.insertMallSpec(mallSpec);
        if (mallSpec.getValues().size() > 0) {
            /**获取生成雪花Id，并赋值给主键，加入至子表对应外键中*/
            mallSpec.setSpecId(mallSpec.getId());
            mallSpecMapper.batchMallSpecValue(mallSpec);
        }
        return rows;
    }

    /**
     * 修改产品规格
     *
     * @param mallSpec 产品规格
     * @return 结果
     */
    @Override
    @Transactional
    public int updateMallSpec(MallSpec mallSpec) {
        mallSpecMapper.deleteMallSpecValueBySpecId(mallSpec);
        if (mallSpec.getValues().size() > 0) {
            mallSpecMapper.batchMallSpecValue(mallSpec);
        }
        return mallSpecMapper.updateMallSpec(mallSpec);
    }

    /**
     * 修改产品规格排序
     *
     * @param mallSpec 产品规格
     * @return 结果
     */
    @Override
    public int updateMallSpecSort(MallSpec mallSpec) {
        return mallSpecMapper.updateMallSpecSort(mallSpec);
    }

    /**
     * 删除产品规格信息
     *
     * @param mallSpec 产品规格
     * @return 结果
     */
    @Override
    @Transactional
    public int deleteMallSpecById(MallSpec mallSpec) {
        mallSpecMapper.deleteMallSpecValueBySpecId(mallSpec);
        return mallSpecMapper.deleteMallSpecById(mallSpec);
    }

    /**
     * 批量删除产品规格
     *
     * @param mallSpec 产品规格
     * @return 结果
     */
    @Override
    @Transactional
    public int deleteMallSpecByIds(MallSpec mallSpec) {
        mallSpecMapper.deleteMallSpecValueBySpecIds(mallSpec);
        return mallSpecMapper.deleteMallSpecByIds(mallSpec);
    }
}