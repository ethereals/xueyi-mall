package com.xueyi.product.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.xueyi.product.mapper.MallTagMapper;
import com.xueyi.product.domain.MallTag;
import com.xueyi.product.service.IMallTagService;

/**
 * 产品标签 业务层处理
 *
 * @author xueyi
 */
@Service
public class MallTagServiceImpl implements IMallTagService
{
    @Autowired
    private MallTagMapper mallTagMapper;

    /**
     * 查询产品标签列表
     *
     * @param mallTag 产品标签
     * @return 产品标签
     */
    @Override
    public List<MallTag> selectMallTagList(MallTag mallTag)
    {
        return mallTagMapper.selectMallTagList(mallTag);
    }


    /**
     * 查询产品标签
     *
     * @param mallTag 产品标签
     * @return 产品标签
     */
    @Override
    public MallTag selectMallTagById(MallTag mallTag)
    {
        return mallTagMapper.selectMallTagById(mallTag);
    }

    /**
     * 新增产品标签
     *
     * @param mallTag 产品标签
     * @return 结果
     */
    @Override
    public int insertMallTag(MallTag mallTag)
    {
        return mallTagMapper.insertMallTag(mallTag);
    }

    /**
     * 修改产品标签
     *
     * @param mallTag 产品标签
     * @return 结果
     */
    @Override
    public int updateMallTag(MallTag mallTag)
    {
        return mallTagMapper.updateMallTag(mallTag);
    }

    /**
     * 修改产品标签排序
     *
     * @param mallTag 产品标签
     * @return 结果
     */
    public int updateMallTagSort(MallTag mallTag)
    {
        return mallTagMapper.updateMallTagSort(mallTag);
    }

    /**
     * 删除产品标签信息
     *
     * @param mallTag 产品标签
     * @return 结果
     */
    @Override
    public int deleteMallTagById(MallTag mallTag)
    {
        return mallTagMapper.deleteMallTagById(mallTag);
    }

    /**
     * 批量删除产品标签
     *
     * @param mallTag 产品标签
     * @return 结果
     */
    @Override
    public int deleteMallTagByIds(MallTag mallTag)
    {
        return mallTagMapper.deleteMallTagByIds(mallTag);
    }
}