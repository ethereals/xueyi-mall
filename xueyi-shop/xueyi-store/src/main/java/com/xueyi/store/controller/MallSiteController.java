package com.xueyi.store.controller;

import java.util.List;
import java.io.IOException;
import javax.servlet.http.HttpServletResponse;

import com.xueyi.common.core.utils.StringUtils;
import com.xueyi.store.api.MallSite;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.xueyi.common.log.annotation.Log;
import com.xueyi.common.log.enums.BusinessType;
import com.xueyi.common.security.annotation.PreAuthorize;
import com.xueyi.store.service.IMallSiteService;
import com.xueyi.common.core.web.controller.BaseController;
import com.xueyi.common.core.web.domain.AjaxResult;

/**
 * 商城-站点模块Controller
 *
 * @author xueyi
 */
@RestController
@RequestMapping("/site")
public class MallSiteController extends BaseController {
    @Autowired
    private IMallSiteService mallSiteService;

    /**
     * 查询商城-站点模块列表
     */
    @PreAuthorize(hasPermi = "store:site:list")
    @GetMapping("/list")
    public AjaxResult list(MallSite mallSite) {
        return AjaxResult.success(mallSiteService.selectMallSiteList(mallSite));
    }

    /**
     * 获取商城-站点模块详细信息
     */
    @PreAuthorize(hasPermi = "store:site:query")
    @GetMapping(value = "/byId")
    public AjaxResult getInfo(MallSite mallSite) {
        return AjaxResult.success(mallSiteService.selectMallSiteById(mallSite));
    }

    /**
     * 新增商城-站点模块
     */
    @PreAuthorize(hasPermi = "store:site:add")
    @Log(title = "商城-站点模块", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody MallSite mallSite) {
        return toAjax(mallSiteService.insertMallSite(mallSite));
    }

    /**
     * 修改商城-站点模块
     */
    @PreAuthorize(hasPermi = "store:site:edit")
    @Log(title = "商城-站点模块", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody MallSite mallSite) {
        return toAjax(mallSiteService.updateMallSite(mallSite));
    }

    /**
     * 删除商城-站点模块
     */
    @PreAuthorize(hasPermi = "store:site:remove")
    @Log(title = "商城-站点模块", businessType = BusinessType.DELETE)
    @DeleteMapping
    public AjaxResult remove(@RequestBody MallSite mallSite) {
        return toAjax(mallSiteService.deleteMallSiteById(mallSite));
    }
}