package com.xueyi.store.mapper;

import com.xueyi.common.datascope.annotation.DataScope;
import com.xueyi.store.api.MallSite;

import java.util.List;

/**
 * 商城-站点模块Mapper接口
 *
 * @author xueyi
 */
public interface MallSiteMapper {
    /**
     * 查询商城-站点模块
     * 访问控制 s 租户查询
     *
     * @param mallSite 商城-站点模块
     * @return 商城-站点模块
     */
    @DataScope(SWeAlias = "s")
    public MallSite selectMallSiteById(MallSite mallSite);

    /**
     * 查询商城-站点模块列表
     * 访问控制 s 租户查询
     *
     * @param mallSite 商城-站点模块
     * @return 商城-站点模块集合
     */
    @DataScope(SeAlias = "s")
    public List<MallSite> selectMallSiteList(MallSite mallSite);

    /**
     * 新增商城-站点模块
     * 访问控制 empty 租户更新（无前缀）
     *
     * @param mallSite 商城-站点模块
     * @return 结果
     */
    @DataScope(SWueAlias = "empty")
    public int insertMallSite(MallSite mallSite);

    /**
     * 修改商城-站点模块
     * 访问控制 empty 租户更新（无前缀）
     *
     * @param mallSite 商城-站点模块
     * @return 结果
     */
    @DataScope(SWueAlias = "empty")
    public int updateMallSite(MallSite mallSite);

    /**
     * 删除商城-站点模块
     * 访问控制 empty 租户更新（无前缀）
     *
     * @param mallSite 商城-站点模块
     * @return 结果
     */
    @DataScope(SWueAlias = "empty")
    public int deleteMallSiteById(MallSite mallSite);
}