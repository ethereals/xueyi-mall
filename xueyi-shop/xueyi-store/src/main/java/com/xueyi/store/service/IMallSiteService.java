package com.xueyi.store.service;

import java.util.List;

import com.xueyi.store.api.MallSite;

/**
 * 商城-站点模块Service接口
 *
 * @author xueyi
 */
public interface IMallSiteService {
    /**
     * 查询商城-站点模块
     *
     * @param mallSite 商城-站点模块
     * @return 商城-站点模块
     */
    public MallSite selectMallSiteById(MallSite mallSite);

    /**
     * 查询商城-站点模块列表
     *
     * @param mallSite 商城-站点模块
     * @return 商城-站点模块集合
     */
    public List<MallSite> selectMallSiteList(MallSite mallSite);

    /**
     * 新增商城-站点模块
     *
     * @param mallSite 商城-站点模块
     * @return 结果
     */
    public int insertMallSite(MallSite mallSite);

    /**
     * 修改商城-站点模块
     *
     * @param mallSite 商城-站点模块
     * @return 结果
     */
    public int updateMallSite(MallSite mallSite);

    /**
     * 删除商城-站点模块信息
     *
     * @param mallSite 商城-站点模块
     * @return 结果
     */
    public int deleteMallSiteById(MallSite mallSite);
}