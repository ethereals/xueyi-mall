package com.xueyi.store.service.impl;

import java.util.List;

import com.xueyi.common.core.utils.DateUtils;
import com.xueyi.store.api.MallSite;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.xueyi.store.mapper.MallSiteMapper;
import com.xueyi.store.service.IMallSiteService;

/**
 * 商城-站点模块Service业务层处理
 *
 * @author xueyi
 */
@Service
public class MallSiteServiceImpl implements IMallSiteService {
    @Autowired
    private MallSiteMapper mallSiteMapper;

    /**
     * 查询商城-站点模块
     *
     * @param mallSite 商城-站点模块
     * @return 商城-站点模块
     */
    @Override
    public MallSite selectMallSiteById(MallSite mallSite) {
        return mallSiteMapper.selectMallSiteById(mallSite);
    }

    /**
     * 查询商城-站点模块列表
     *
     * @param mallSite 商城-站点模块
     * @return 商城-站点模块
     */
    @Override
    public List<MallSite> selectMallSiteList(MallSite mallSite) {
        return mallSiteMapper.selectMallSiteList(mallSite);
    }

    /**
     * 新增商城-站点模块
     *
     * @param mallSite 商城-站点模块
     * @return 结果
     */
    @Override
    public int insertMallSite(MallSite mallSite) {
        return mallSiteMapper.insertMallSite(mallSite);
    }

    /**
     * 修改商城-站点模块
     *
     * @param mallSite 商城-站点模块
     * @return 结果
     */
    @Override
    public int updateMallSite(MallSite mallSite) {
        return mallSiteMapper.updateMallSite(mallSite);
    }

    /**
     * 删除商城-站点模块信息
     *
     * @param mallSite 商城-站点模块
     * @return 结果
     */
    @Override
    public int deleteMallSiteById(MallSite mallSite) {
        return mallSiteMapper.deleteMallSiteById(mallSite);
    }
}